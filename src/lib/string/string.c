#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <lib/string.h>

static char *uint32_to_hex32(char *buf, uint32_t val)
{
        char index[16] = {
            '0',
            '1',
            '2',
            '3',
            '4',
            '5',
            '6',
            '7',
            '8',
            '9',
            'a',
            'b',
            'c',
            'd',
            'e',
            'f'};
        bool first_non_zero = 0;
        size_t k = 0;
        for (size_t i = 0, j = sizeof(uint32_t) * 8 - 4; i < sizeof(uint32_t) * 2; i++, j -= 4)
        {
                char c = index[((val & (((uint32_t)0xF) << j)) >> j)];

                // This is done for removing any leading zeroes.

                if (c != '0')
                {
                        first_non_zero = 1;
                }
                if (first_non_zero)
                {
                        buf[k++] = c;
                }
        }
        buf[k] = '\0';
        return buf;
}

int memcmp(const void *aptr, const void *bptr, size_t size)
{
        const unsigned char *a = (const unsigned char *)aptr;
        const unsigned char *b = (const unsigned char *)bptr;
        for (size_t i = 0; i < size; i++)
        {
                if (a[i] < b[i])
                {
                        return -1;
                }
                else if (b[i] < a[i])
                {
                        return 1;
                }
        }
        return 0;
}

void *memcpy(void *restrict dstptr, const void *restrict srcptr, size_t size)
{
        unsigned char *dst = (unsigned char *)dstptr;
        const unsigned char *src = (const unsigned char *)srcptr;
        for (size_t i = 0; i < size; i++)
        {
                dst[i] = src[i];
        }
        return dstptr;
}

void *memmove(void *dstptr, const void *srcptr, size_t size)
{
        unsigned char *dst = (unsigned char *)dstptr;
        const unsigned char *src = (const unsigned char *)srcptr;
        if (dst < src)
        {
                for (size_t i = 0; i < size; i++)
                        dst[i] = src[i];
        }
        else
        {
                for (size_t i = size; i != 0; i--)
                {
                        dst[i - 1] = src[i - 1];
                }
        }
        return dstptr;
}

void *memset(void *bufptr, int value, size_t size)
{
        unsigned char *buf = (unsigned char *)bufptr;
        for (size_t i = 0; i < size; i++)
        {
                buf[i] = (unsigned char)value;
        }
        return bufptr;
}

size_t strlen(const char *str)
{
        size_t len = 0;
        while (str[len])
        {
                len++;
        }
        return len;
}

int strcmp(const char *s1, const char *s2)
{
        const unsigned char *a = (const unsigned char *)s1;
        const unsigned char *b = (const unsigned char *)s2;
        while (*a && (*a == *b))
        {
                a++, b++;
        }
        return *a - *b;
}

char *strcpy(char *restrict dstptr, const char *restrict srcptr)
{
        unsigned char *dst = (unsigned char *)dstptr;
        const unsigned char *src = (const unsigned char *)srcptr;
        size_t i = 0;
        while ((dst[i] = src[i]) != '\0')
        {
                i++;
        }
        return dstptr;
}

char *strncpy(char *restrict dstptr, const char *restrict srcptr, size_t amount)
{
        unsigned char *dst = (unsigned char *)dstptr;
        const unsigned char *src = (const unsigned char *)srcptr;
        for (size_t i = 0; i < amount; i++)
        {
                dst[i] = src[i];
        }
        return dstptr;
}

static uint32_t hex_to_int(char *str)
{
        uint32_t res = 0;
        while (*str)
        {
                uint8_t byte = *(str++);
                if (byte >= '0' && byte <= '9')
                {
                        byte = byte - '0';
                }
                else if (byte >= 'a' && byte <= 'f')
                {
                        byte = byte - 'a' + 10;
                }
                else if (byte >= 'A' && byte <= 'F')
                {
                        byte = byte - 'A' + 10;
                }
                res = (res << 4) | (byte & 0xF);
        }
        return res;
}

uint32_t atoi(char *str, uint8_t base)
{
        uint32_t res = 0;
        if (base == 10)
        {
                size_t len = strlen(str);
                for (size_t i = 0; i < len; i++)
                {
                        res = res * 10 + str[i] - '0';
                }
        }
        else if (base == 16)
        {
                res = hex_to_int(str);
        }
        return res;
}

char *itoa(char *buf, uint32_t val, uint8_t base)
{
        if (val == 0)
        {
                buf[0] = (val % base) + '0';
                buf[1] = '\0';
                return buf;
        }
        if (base == 16)
        {
                return uint32_to_hex32(buf, val);
        }
        size_t size = 0;
        uint32_t tmp = val;
        while (tmp)
        {
                size++;
                tmp /= base;
        }
        buf[size] = 0;
        while (val)
        {
                buf[--size] = (val % base) + '0';
                val /= base;
        }
        return buf;
}