#include <stdbool.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdint.h>
#include <arch/arch.h>
#include <arch/cpu/cpu.h>
#include <arch/cpu/lapic.h>
#include <arch/cpu/smp.h>
#include <kernel/bootconsole.h>
#include <kernel/common.h>
#include <kernel/printk.h>
#include <kernel/spinlock.h>
#include <lib/string.h>

/*
 * This routine prints an unsigned integer of 32 bits by converting it into its ASCII representation.
 * The basic formula to get the ascii value from the integer value is: value % base + '0'.
 */

static void print_int_u32(uint32_t value, uint8_t base)
{

        // If the number si just one digit print it and return.

        if (value == 0)
        {
                if (likely(bootconsole_is_enabled()))
                {
                        bootconsole_put_char((value % base) + '0');
                }
                return;
        }
        size_t size = 0;
        uint32_t tmp = value;

        // Calculate the size for the buffer to hold the ASCII string of the number.

        while (tmp)
        {
                size++;
                tmp /= base;
        }

        // Add one for the NULL character.
        char buf[size + 1];
        buf[size] = 0;

        // Fill the created buffer in reverse order.

        while (value)
        {
                buf[--size] = (value % base) + '0';
                value /= base;
        }

        // Finally print the ASCII string containing the number.

        if (likely(bootconsole_is_enabled()))
        {
                bootconsole_put_string(&buf[0], strlen(&buf[0]));
        }
}

// This routine is the same as the unsigned one except that it checks if the number has a sign and prepends it prior to printing.

static void print_int_32(int32_t value, uint8_t base)
{
        if (value < 0)
        {
                if (likely(bootconsole_is_enabled()))
                {
                        bootconsole_put_char('-');
                }
        }
        value = (value < 0 ? -value : value);
        if (value == 0)
        {
                if (likely(bootconsole_is_enabled()))
                {
                        bootconsole_put_char((value % base) + '0');
                }
                return;
        }
        size_t size = 0;
        int32_t tmp = value;
        while (tmp)
        {
                size++;
                tmp /= base;
        }
        char buf[size + 1];
        buf[size] = 0;
        while (value)
        {
                buf[--size] = (value % base) + '0';
                value /= base;
        }
        if (likely(bootconsole_is_enabled()))
        {
                bootconsole_put_string(&buf[0], strlen(&buf[0]));
        }
}

// This routine is equivalent to print_int_u32 but for 64 bits numbers.

static void print_int_u64(uint64_t value, uint8_t base)
{
        if (value == 0)
        {
                if (likely(bootconsole_is_enabled()))
                {
                        bootconsole_put_char((value % base) + '0');
                }
                return;
        }
        size_t size = 0;
        uint64_t tmp = value;
        while (tmp)
        {
                size++;
                tmp /= base;
        }
        char buf[size + 1];
        buf[size] = 0;
        while (value)
        {
                buf[--size] = (value % base) + '0';
                value /= base;
        }
        if (likely(bootconsole_is_enabled()))
        {
                bootconsole_put_string(&buf[0], strlen(&buf[0]));
        }
}

// This routine is equivalent to print_int_32 but for 64 bit numbers.

static void print_int_64(int64_t value, uint8_t base)
{
        if (value < 0)
        {
                if (likely(bootconsole_is_enabled()))
                {
                        bootconsole_put_char('-');
                }
        }
        value = (value < 0 ? -value : value);
        if (value == 0)
        {
                if (likely(bootconsole_is_enabled()))
                {
                        bootconsole_put_char((value % base) + '0');
                }
                return;
        }
        size_t size = 0;
        int64_t tmp = value;
        while (tmp)
        {
                size++;
                tmp /= base;
        }
        char buf[size + 1];
        buf[size] = 0;
        while (value)
        {
                buf[--size] = (value % base) + '0';
                value /= base;
        }
        if (likely(bootconsole_is_enabled()))
        {
                bootconsole_put_string(&buf[0], strlen(&buf[0]));
        }
}

/*
 * This routine prints a 32 or 64 bit integer as hexadecimal number.
 * It works by checking what value of the 32/64 bit word has each single nibble.
 * Then that value is used as an index into the index array which contains the ASCII value for 0 to F.
 */

static void print_hex(uint32_t value, bool sixty_four_bits)
{
        char index[16] = {
            '0',
            '1',
            '2',
            '3',
            '4',
            '5',
            '6',
            '7',
            '8',
            '9',
            'A',
            'B',
            'C',
            'D',
            'E',
            'F'};
        if (likely(bootconsole_is_enabled()))
        {
                bootconsole_put_char('0');
                bootconsole_put_char('x');
        }
        bool first_non_zero = 0;
        if (!sixty_four_bits)
        {
                for (size_t i = 0, j = sizeof(uint32_t) * 8 - 4; i < sizeof(uint32_t) * 2; i++, j -= 4)
                {
                        char c = index[((value & (((uint32_t)0xF) << j)) >> j)];

                        // This is done for removing any leading zeroes.

                        if (c != '0')
                        {
                                first_non_zero = 1;
                        }
                        if (first_non_zero)
                        {
                                if (likely(bootconsole_is_enabled()))
                                {
                                        bootconsole_put_char(c);
                                }
                        }
                }
        }
        else
        {
                for (size_t i = 0, j = sizeof(uint64_t) * 8 - 4; i < sizeof(uint64_t) * 2; i++, j -= 4)
                {
                        char c = index[((value & (((uint64_t)0xF) << j)) >> j)];
                        // This is done for removing any leading zeroes.
                        if (c != '0')
                        {
                                first_non_zero = 1;
                        }
                        if (first_non_zero)
                        {
                                if (likely(bootconsole_is_enabled()))
                                {
                                        bootconsole_put_char(c);
                                }
                        }
                }
        }
        if (!first_non_zero)
        {
                if (likely(bootconsole_is_enabled()))
                {
                        bootconsole_put_char('0');
                }
        }
}

/*
 * This function is not used directly. See printk.h for the definition of appropriate macros to use instead.
 * This simply scans the format string for known and implemented patterns and calls the appropriate functions
 * to print the corresponding values. If the format string is just a normal string, it is interpreted as such and
 * printed.
 */

void _printk(bool panic, const char *restrict format, ...)
{
        va_list parameters;
        va_start(parameters, format);

        // While format is not a NULL character
        while (*format)
        {

                // This first case handles printing the format string from the first character up to % excluded as a normal string.

                if (format[0] != '%' || format[1] == '%')
                {
                        if (format[0] == '%')
                        {
                                format++;
                        }
                        size_t amount = 1;
                        while (format[amount] && format[amount] != '%')
                        {
                                amount++;
                        }
                        if (likely(bootconsole_is_enabled()))
                        {
                                bootconsole_put_string(format, amount);
                        }
                        format += amount;
                        continue;
                }

                // Save the position after the % character in case it isn't one of those below (meeaning it's not implemented).

                const char *format_begun_at = format++;
                switch (*format)
                {
                case 'c':
                        format++;
                        char c = (char)va_arg(parameters, int);
                        if (likely(bootconsole_is_enabled()))
                        {
                                bootconsole_put_char(c);
                        }
                        break;
                case 'd':
                {
                        format++;
                        uint32_t d = (uint32_t)va_arg(parameters, uint32_t);
                        print_int_u32(d, 10);
                }
                break;
                case 'b':
                {
                        format++;
                        uint32_t d = (uint32_t)va_arg(parameters, uint32_t);
                        print_int_u32(d, 2);
                }
                break;
                case 'x':
                {
                        format++;
                        uint32_t d = (uint32_t)va_arg(parameters, uint32_t);
                        print_hex(d, false);
                }
                break;
                case 'l':
                        switch (*(format + 1))
                        {
                        case 'd':
                        {
                                format += 2;
                                uint64_t d = (uint64_t)va_arg(parameters, uint64_t);
                                print_int_u64(d, 10);
                        }
                        break;
                        case 'b':
                        {
                                format += 2;
                                uint64_t d = (uint64_t)va_arg(parameters, uint64_t);
                                print_int_u64(d, 2);
                        }
                        break;
                        case 'x':
                        {
                                format += 2;
                                uint64_t d = (uint64_t)va_arg(parameters, uint64_t);
                                print_hex(d, true);
                        }
                        break;
                        }
                        break;
                case 's':
                        switch (*(format + 1))
                        {
                        case 'd':
                        {
                                format += 2;
                                int32_t d = (int32_t)va_arg(parameters, int32_t);
                                print_int_32(d, 10);
                        }
                        break;
                        case 'l':
                        {
                                if (*(format + 2) == 'd')
                                {
                                        format += 3;
                                        int64_t d = (int64_t)va_arg(parameters, int64_t);
                                        print_int_64(d, 10);
                                }
                        }
                        break;
                        default:
                                format++;
                                const char *str = va_arg(parameters, const char *);
                                if (likely(bootconsole_is_enabled()))
                                {
                                        bootconsole_put_string(str, strlen(str));
                                }
                        }
                        break;
                default:
                        format = format_begun_at;
                        size_t len = strlen(format);
                        if (likely(bootconsole_is_enabled()))
                        {
                                bootconsole_put_string(format, len);
                        }
                        format += len;
                }
        }
        va_end(parameters);

        // This implements the panic() macro like function and halts the machine after the printout is done.

        if (panic)
        {
                /*
                 * This just lets one CPU panic.
                 * TODO: send halting interprocessor interrupt on SMP systems.
                 */
        #ifdef SMP
                for (size_t i = 0; i < num_cpus; i++)
                {
                        if (i != cpu->cpu_id)
                        {
                                lapic_send_ipi(cpu_data[i].cpu_id, (uint8_t)49 | LAPIC_ICR_DELIVERY_MODE_FIXED | LAPIC_ICR_DESTINATION_MODE_PHYSICAL | LAPIC_ICR_DESTINATION_NO_SHORTHAND);
                                do
                                {
                                        arch_halt();
                                } while (LAPIC_ICR_DELIVERY_STATUS(lapic_read(LAPIC_INTERRUPT_COMMAND_REGISTER_0)) != LAPIC_ICR_DELIVERY_STATUS_IDLE);
                        }
                }
        #endif /* SMP */

                unlock(&bootconsole_lock);
                _arch_disable_interrupts();

                while (1)
                {
                        arch_halt();
                }
        }
}