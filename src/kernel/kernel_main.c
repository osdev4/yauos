#include <stddef.h>
#include <stdint.h>
#include <arch/cpu/cpu.h>
#include <kernel/bootinfo.h>
#include <kernel/printk.h>
#include <kernel/mm/kmalloc.h>
#include <lib/string.h>

__attribute__((noinline)) void test(const char* arg)
{
        char buffer[10] = {0};
        strncpy(buffer, arg, 200);
}

void kernel_main()
{
        lock(&bootconsole_lock);
        printk("[KERNEL]: Arch init complete.\n[KERNEL]: Command line: \"%s\"\n", boot_info->command_line);
        unlock(&bootconsole_lock);
        if (k_malloc_init())
        {
                lock(&bootconsole_lock);
                panic("[KERNEL]: Failed to initialize heap! File: %s line: %d function: %s\n", __FILENAME__, __LINE__, __func__);
        }
        size_t count = 0;
        for (;;)
        {
                uint32_t *p = (uint32_t *)k_malloc(sizeof(uint32_t));
                if (p)
                {
                        *p = 0xDEADBEEF;
                        if (*p != 0xDEADBEEF)
                        {
                                count++;
                        }
                }
                else
                {
                        break;
                }
        }
        lock(&bootconsole_lock);
        printk("Allocated whole heap!\nTotal failed memory readbacks: %d\n", count);
        unlock(&bootconsole_lock);
        extern void delay_l(uint64_t);
        test("ciao coglione, come stai? eccoti un bel stack smash!\n");
#ifdef SMP
        while (1)
        {
                delay_l(10000);
                lock(&bootconsole_lock);
                printk("CPU (%d): 1 second has elapsed!\n", cpu->cpu_id);
                unlock(&bootconsole_lock);
        }
#endif
}