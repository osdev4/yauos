#include <arch/cpu/cpu.h>
#ifdef SMP
#include <arch/cpu/smp.h>
#endif
#include <kernel/spinlock.h>

inline void lock(spinlock_t *lock)
{
        arch_lock(lock);
}

inline void unlock(spinlock_t *lock)
{
        arch_unlock(lock);
}