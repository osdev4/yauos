#include <arch/cpu/gdt.h>

.section .text
	.global load_gdt
	.type load_gdt, @function
	load_gdt:
		movl 4(%esp), %eax
		lgdtl (%eax)
		
                // This is necessary to load the code segment selector CS with the kernel code segment, which is at offset 0x8 into the gdt.
		
		ljmpl $(GDT_KERNEL_CODE_OFFSET), $0f

                0:

                        // Loading kernel data segment offset which is at 0x10 into the gdt and using it for all data segment selectors.
                        xorl %eax, %eax
                        movw $(GDT_KERNEL_DATA_OFFSET), %ax
                        movw %ax, %ds
                        movw %ax, %es
                        movw %ax, %fs
                        movw %ax, %ss
                        #ifdef SMP
                        movw $(GDT_KERNEL_PER_CPU_DATA_OFFSET), %ax
                        #endif
                        movw %ax, %gs
                        ret

	.size load_gdt, . - load_gdt