#include <stddef.h>
#include <stdint.h>
#include <arch/cpu/cpu.h>
#include <arch/cpu/exception_interrupt.h>
#include <arch/cpu/gdt.h>
#include <arch/cpu/io.h>
#ifdef SMP
#include <arch/cpu/smp.h>
#endif
#include <arch/kernel/mm/vm.h>
#include <kernel/bootinfo.h>
#include <kernel/printk.h>
#include <lib/string.h>

/*
 * Defined in stack_trace.S
 * TODO: implement loading kernel symbols and function name lookups for stack traces.
 */

extern uint32_t walk_stack(uint32_t *array, uint32_t number_frames);

static void debug_dump(exception_context_t *context)
{
        uint32_t stack_trace[10];
        memset(stack_trace, 0x0, sizeof(uint32_t) * 10);
        uint32_t n_frames = walk_stack(stack_trace, 10);
        lock(&bootconsole_lock);
        printk("[KERNEL]: Exception number %x with error code: %x occurred while running kernel code on CPU %x\nRegister dump:\nEAX = %x\nEBX = %x\nECX = %x\nEDX = %x\nESI = %x\nEDI = %x\nEBP = %x\nEIP = %x\nCS = %x\nSS = %x\nDS = %x\nES = %x\nFS = %x\nGS = %x\nEFLAGS = %x\nBack trace:\n", context->number, context->error_code, cpu->cpu_id, context->eax, context->ebx, context->ecx, context->edx, context->esi, context->edi, context->ebp, context->eip, context->cs, context->kernel_ss, context->ds, context->es, context->fs, context->gs, context->eflags);
        for (size_t i = 0; i < n_frames; i++)
        {
                uint32_t max = 0;
                uint32_t offset_from_frame = 0;
                size_t j;
                size_t k = 0;
                for (j = 0; j < boot_info->symbol_table_entries; j++)
                {
                        if (boot_info->symbol_table[j].address > max && boot_info->symbol_table[j].address < stack_trace[i])
                        {
                                max = boot_info->symbol_table[j].address;
                                k = j;
                        }
                }
                char *function_name = NULL;
                if (max != 0)
                {
                        function_name = boot_info->symbol_table[k].function_name;
                        offset_from_frame = stack_trace[i] - max;
                }
                printk("[%x]: [%x] ---> [%s + %x]\n", i, stack_trace[i], function_name == NULL ? "unknown symbol" : function_name, offset_from_frame);
        }
        panic("[KERNEL]: End of trace.\n");
}

void exception_common_handler(exception_context_t *context)
{
        switch (context->number)
        {
        case 14:
                if (context->cs == GDT_USER_CODE_OFFSET)
                {
                        do_page_fault(read_cr2(), cpu->cpu_id);
                }
                else
                {
                        lock(&bootconsole_lock);
                        printk("[KERNEL]: page fault occurred in kernel code at address: %x\n", read_cr2());
                        unlock(&bootconsole_lock);
                        debug_dump(context);
                }
                break;
        default:
                if (context->cs == GDT_USER_CODE_OFFSET)
                {
                        // TODO: when user space is implemented send the appropriate signal to the offending process.
                }
                else
                {
                        debug_dump(context);
                }
        }
}