#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <arch/mmu.h>
#include <kernel/bootconsole.h>
#include <lib/string.h>
#include "vga_text_mode.h"

static uint16_t *const VGA_MEMORY = (uint16_t *)PHYSICAL_TO_VIRTUAL(0xB8000);
static size_t vga_row;
static size_t vga_column;
static uint8_t vga_color;
static uint8_t vga_fg_color;
static uint8_t vga_bg_color;
static uint16_t *vga_buffer;

int vga_init()
{
        vga_row = 0;
        vga_column = 0;
        vga_fg_color = VGA_COLOR_WHITE;
        vga_bg_color = VGA_COLOR_BLACK;
        vga_color = vga_entry_color(vga_fg_color, vga_bg_color);
        vga_buffer = VGA_MEMORY;
        for (size_t row = 0; row < VGA_HEIGHT; row++)
        {
                for (size_t column = 0; column < VGA_WIDTH; column++)
                {
                        vga_buffer[row * VGA_WIDTH + column] = vga_entry(' ', vga_color);
                }
        }
        return 0;
}

static void scroll_up()
{
        for (size_t i = 0; i < VGA_HEIGHT; i++)
        {
                memcpy(&vga_buffer[i * VGA_WIDTH], &vga_buffer[(i + 1) * VGA_WIDTH], sizeof(uint16_t) * VGA_WIDTH);
        }       
}

static void advance_and_scroll_if_needed()
{
        if (++vga_column == VGA_WIDTH)
        {
                vga_column = 0;
                if ((vga_row + 1) == VGA_HEIGHT)
                {
                        scroll_up();
                        memset(&vga_buffer[vga_row * VGA_WIDTH], 0x0, sizeof(uint16_t) * VGA_WIDTH);
                }
                else
                {
                        vga_row++;
                }
        }
}

static inline uint8_t vga_entry_color(enum vga_color foreground, enum vga_color background)
{
        return foreground | (background << 4);
}

static inline uint16_t vga_entry(unsigned char uc, uint8_t color)
{
        return (uint16_t)(uc) | (((uint16_t)(color)) << 8);
}

static void _vga_put_char(uint8_t c, uint8_t color, size_t row, size_t column)
{
        vga_buffer[row * VGA_WIDTH + column] = vga_entry(c, color);
}

void vga_put_char(char c)
{
        uint8_t uc = c;
        if (uc == '\n')
        {
                vga_column = 0;
                if ((vga_row + 1) == VGA_HEIGHT)
                {
                        scroll_up();
                        memset(&vga_buffer[vga_row * VGA_WIDTH], 0x0, sizeof(uint16_t) * VGA_WIDTH);
                }
                else
                {
                        vga_row++;
                }
        }
        else if (uc == '\t')
        {
                for (size_t i = 0; i < VGA_TAB_SIZE; i++)
                {
                        _vga_put_char(' ', vga_color, vga_row, vga_column);
                        advance_and_scroll_if_needed();
                }
        }
        else
        {
                _vga_put_char(uc, vga_color, vga_row, vga_column);
                advance_and_scroll_if_needed();
        }
}

void vga_put_string(const char *s, size_t size)
{
        for (size_t i = 0; i < size; i++)
        {
                vga_put_char(s[i]);
        }
}

bootconsole_t bootconsole_vga_text_mode = {
    .console_init = vga_init,
    .console_put_char = vga_put_char,
    .console_put_string = vga_put_string,
};