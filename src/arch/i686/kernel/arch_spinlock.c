#include <stdbool.h>
#include <arch/cpu/cpu.h>
#ifdef SMP
#include <arch/cpu/smp.h>
#endif
#include <arch/kernel/spinlock.h>
#include <kernel/common.h>
#include <kernel/printk.h>

void arch_disable_interrupts(void)
{
        uint32_t eflags = read_eflags();
        _arch_disable_interrupts();
        if (cpu->number_cli++ == 0)
        {
                cpu->interrupts_enabled = eflags & (1 << EFLAGS_INTERRUPT_ENABLE_FLAG_SHIFT);
        }
}

void arch_enable_interrupts(void)
{
        if (read_eflags() & (1 << EFLAGS_VIRTUAL_INTERRUPT_FLAG_SHIFT))
        {
                arch_disable_interrupts();
                while (1)
                {
                        arch_halt();
                }
        }
        if (--cpu->number_cli < 0)
        {
                arch_disable_interrupts();
                while (1)
                {
                        arch_halt();
                }
        }
        if (cpu->number_cli == 0 && cpu->interrupts_enabled)
        {
                _arch_enable_interrupts();
        }
}

void arch_lock(spinlock_t *lock)
{

        if (likely(lock->locking))
        {
                arch_disable_interrupts();

#ifdef SMP

                // This avoids deadlock if one cpu tries to acquire the same lock multiple times.

                if (lock->cpu_id == cpu->cpu_id)
                {
                        lock->nested++;
                        arch_enable_interrupts();
                }
                else
                {
                        while (_xchg(1, &(lock->lock)) != 0)
                                ;
                        lock->cpu_id = cpu->cpu_id;
                        lock->nested++;
                        lock->locked = true;
                        arch_enable_interrupts();
                }
#endif
        }
}

void arch_unlock(spinlock_t *lock)
{
        if (likely(lock->locking))
        {
#ifdef SMP
                if (lock->cpu_id != cpu->cpu_id)
                {
                        arch_disable_interrupts();
                        while (1)
                        {
                                arch_halt();
                        }
                }
                arch_disable_interrupts();
                if (--lock->nested == 0)
                {
                        lock->cpu_id = 0xFF;
                        lock->locked = false;
                        _xchg(0, &(lock->lock));
                        arch_enable_interrupts();
                }
#else
                arch_enable_interrupts();
#endif
        }
}