#include <stddef.h>
#include <stdint.h>
#include <arch/align.h>
#include <arch/kernel/mm/vm.h>
#include <arch/types.h>
#include <kernel/bootconsole.h>
#include <kernel/bootmem.h>
#include <kernel/common.h>
#include <kernel/printk.h>
#include <kernel/mm/pm.h>
#include <lib/string.h>
#include <platform/multiboot2.h>
#include "bootconsole_framebuffer.h"

static bootconsole_framebuffer_data_t framebuffer_info;
void *framebuffer = (void *)0xE0000000;
void *frame_buffer_back_buffer = NULL;
static uint16_t framebuffer_row = 0;
static uint16_t framebuffer_column = 0;
static size_t framebuffer_rows = 0;
static size_t framebuffer_columns = 0;
static uint32_t framebuffer_foreground_color = 0;
static uint32_t framebuffer_background_color = 0;
psf_font_t *font = (psf_font_t *)&_binary_console_font_start;
static uint16_t *unicode;

int bootconsole_framebuffer_init(void *data)
{
        multiboot2_information_header_t *m_boot2_info = (multiboot2_information_header_t *)data;
        for (multiboot2_tag_header_t *tag = (multiboot2_tag_header_t *)((uintptr_t)(m_boot2_info) + 8); tag->type != MULTIBOOT2_TAG_END_TYPE;)
        {

                // TODO: Better handle corner cases for the kernel command line errors.

                if (tag->type == MULTIBOOT2_TAG_FRAMEBUFFER_INFO_TYPE)
                {
                        multiboot2_tag_framebuffer_info_t *multiboot2_framebuffer_info = (multiboot2_tag_framebuffer_info_t *)tag;

                        // Supporting only framebuffers with direct RGB for now.

                        if (multiboot2_framebuffer_info->framebuffer_type != 1)
                        {
                                return -1;
                        }
                        framebuffer_info.framebuffer_phys_address = (phys_addr_t)multiboot2_framebuffer_info->framebuffer_addr;
                        framebuffer_info.framebuffer_virt_address = 0xE0000000;
                        framebuffer_info.framebuffer_width = multiboot2_framebuffer_info->framebuffer_width;
                        framebuffer_info.framebuffer_height = multiboot2_framebuffer_info->framebuffer_height;
                        framebuffer_info.framebuffer_pitch = multiboot2_framebuffer_info->framebuffer_pitch;
                        framebuffer_info.framebuffer_bpp = multiboot2_framebuffer_info->framebuffer_bpp;
                        framebuffer_info.framebuffer_red_position = multiboot2_framebuffer_info->color_type.framebuffer_red_field_position;
                        framebuffer_info.framebuffer_red_mask = multiboot2_framebuffer_info->color_type.framebuffer_red_mask_size;
                        framebuffer_info.framebuffer_green_position = multiboot2_framebuffer_info->color_type.framebuffer_green_field_position;
                        framebuffer_info.framebuffer_green_mask = multiboot2_framebuffer_info->color_type.framebuffer_green_mask_size;
                        framebuffer_info.framebuffer_blue_position = multiboot2_framebuffer_info->color_type.framebuffer_blue_field_position;
                        framebuffer_info.framebuffer_blue_mask = multiboot2_framebuffer_info->color_type.framebuffer_blue_mask_size;
                        framebuffer_rows = framebuffer_info.framebuffer_height / font->height;
                        framebuffer_columns = framebuffer_info.framebuffer_width / (font->width + 1);

                        // Map the framebuffer
                        size_t framebuffer_size = framebuffer_info.framebuffer_pitch * framebuffer_info.framebuffer_height;
                        phys_addr_t framebuffer_phys_end = framebuffer_info.framebuffer_phys_address + framebuffer_size;
                        virt_addr_t framebuffer_vir_start = 0xE0000000;

                        // Add a secondary in-memory copy of the framebuffer to speed up scrolling.
                        virt_addr_t framebuffer_back_buffer_vir_start = framebuffer_vir_start + framebuffer_size;
                        frame_buffer_back_buffer = (void *)framebuffer_back_buffer_vir_start;

                        for (uint32_t s = framebuffer_info.framebuffer_phys_address; s < framebuffer_phys_end; s += 4096, framebuffer_vir_start += 4096, framebuffer_back_buffer_vir_start += 4096)
                        {
                                if (map_page(s, framebuffer_vir_start, PROT_PRESENT | PROT_KERN | PROT_READ_WRITE | PROT_CACHE_DISABLE, false))
                                {
                                        while (1)
                                        {
                                                arch_halt();
                                        }
                                }
                                phys_addr_t frame = get_free_frame();
                                if (frame == (phys_addr_t)-1)
                                {
                                        while (1)
                                        {
                                                arch_halt();
                                        }
                                }
                                if (map_page(frame, framebuffer_back_buffer_vir_start, PROT_PRESENT | PROT_KERN | PROT_READ_WRITE, false))
                                {
                                        while (1)
                                        {
                                                arch_halt();
                                        }
                                }
                        }

                        // Set default background and foreground colors.

                        framebuffer_set_color(BLACK, WHITE);

                        // Set the background color

                        for (uint32_t *s = (uint32_t *)framebuffer, *s2 = (uint32_t *)frame_buffer_back_buffer; (uintptr_t)s < framebuffer_vir_start && (uintptr_t)s2 < framebuffer_back_buffer_vir_start; s++, s2++)
                        {
                                *s = framebuffer_background_color;
                                *s2 = framebuffer_background_color;
                        }
                }

                // Multiboot2 tags are 8 byte aligned. See multiboo2.h for more info.

                tag = (multiboot2_tag_header_t *)ALIGN(((uintptr_t)tag + tag->size), 8);
        }
        psf_unicode_table_init();
        return 0;
}

static void psf_unicode_table_init()
{
        uint16_t glyph = 0;
        if (!(font->flags & PSF_FLAG_UNICODE_PRESENT))
        {
                unicode = NULL;
                return;
        }
        uint8_t *s = (uint8_t *)((uintptr_t)&_binary_console_font_start + font->header_size + font->number_of_glyphs * font->bytes_per_glyph);
        unicode = (uint16_t *)b_malloc(sizeof(uint16_t) * USHRT_MAX);
        if (!unicode)
        {
                panic("[KERNEL]: Failed to allocate memory! File: %s line: %d function: %s\n", __FILENAME__, __LINE__, __func__);
        }
        while ((uintptr_t)s < (uintptr_t)&_binary_console_font_end)
        {
                uint16_t uc = (uint16_t) * (uint8_t *)s;
                if (uc == 0xFF)
                {
                        glyph++;
                        s++;
                        continue;
                }
                else if (uc & 0x80)
                {
                        if ((uc & 0x20) == 0)
                        {
                                uc = ((s[0] & 0x1F) << 6) + (s[1] & 0x3F);
                                s++;
                        }
                        else if ((uc & 0x10) == 0)
                        {
                                uc = ((((s[0] & 0xF) << 6) + (s[1] & 0x3F)) << 6) + (s[2] & 0x3F);
                                s += 2;
                        }
                        else if ((uc & 0x8) == 0)
                        {
                                uc = ((((((s[0] & 0x7) << 6) + (s[1] & 0x3F)) << 6) + (s[2] & 0x3F)) << 6) + (s[3] & 0x3F);
                                s += 3;
                        }
                        else
                        {
                                uc = 0;
                        }
                }
                unicode[uc] = glyph;
                s++;
        }
}

static void framebuffer_flush_buffer()
{
        memcpy(framebuffer, frame_buffer_back_buffer, framebuffer_info.framebuffer_pitch * framebuffer_info.framebuffer_height);
}

static void framebuffer_put_char(uint16_t c, uint32_t row, uint32_t column)
{
        uint32_t bytes_per_line = (font->width + 7) / 8;
        if (unicode != NULL)
        {
                c = unicode[c];
        }
        uint8_t *glyph = (uint8_t *)((uintptr_t)&_binary_console_font_start + font->header_size + (c > 0 && c < font->number_of_glyphs ? c : 0) * font->bytes_per_glyph);
        uint32_t offset = (row * font->height * framebuffer_info.framebuffer_pitch) + (column * (font->width + 1) * sizeof(PIXEL));
        uint32_t line;
        uint16_t x, y, mask;
        for (y = 0; y < font->height; y++)
        {
                line = offset;
                mask = 1 << (font->width - 1);
                for (x = 0; x < font->width; x++)
                {
                        *((PIXEL *)((uintptr_t)framebuffer + line)) = *((uint8_t *)glyph) & mask ? framebuffer_foreground_color : framebuffer_background_color;
                        *((PIXEL *)((uintptr_t)frame_buffer_back_buffer + line)) = *((uint8_t *)glyph) & mask ? framebuffer_foreground_color : framebuffer_background_color;
                        mask >>= 1;
                        line += sizeof(PIXEL);
                }

                /*
                   Set the remaining space pixel to the background color.
                   Here we directly write to both the back buffer and front buffer because it makes no sense to do a framebuffer flush for every character when there's potentially no need for scrolling (and hence copying the lines around).
                 */
                *((PIXEL *)((uintptr_t)framebuffer + line)) = framebuffer_background_color;
                *((PIXEL *)((uintptr_t)frame_buffer_back_buffer + line)) = framebuffer_background_color;
                glyph += bytes_per_line;
                offset += framebuffer_info.framebuffer_pitch;
        }
}

/*
  The scrolling of the framebuffer is performed on the back_buffer because reading from video memory is painfully slow on real hardware,
  whereas on Qemu it is fast due to the host OS optimiztions and Qemu itself.
  After the scrolling is done the whole back buffer is copied to the video memory avoiding unnecessary reads.
 */
static void scroll_up()
{
        for (size_t i = 0; i < framebuffer_rows - 1; i++)
        {
                memcpy(&((uint8_t *)frame_buffer_back_buffer)[i * framebuffer_info.framebuffer_pitch * font->height], &((uint8_t *)frame_buffer_back_buffer)[(i + 1) * framebuffer_info.framebuffer_pitch * font->height], framebuffer_info.framebuffer_pitch * font->height);
        }
        // Clear the last row with the framebuffer background color.
        uint32_t *last_row = (uint32_t *)&((uint8_t *)frame_buffer_back_buffer)[(framebuffer_rows - 1) * framebuffer_info.framebuffer_pitch * font->height];
        for (size_t i = 0; i < (framebuffer_info.framebuffer_pitch * font->height) / sizeof(uint32_t); i++)
        {
                last_row[i] = framebuffer_background_color;
        }
        framebuffer_flush_buffer();
}

static void advance_and_scroll_if_needed()
{
        if (++framebuffer_column == framebuffer_columns)
        {
                framebuffer_column = 0;
                if ((size_t)(framebuffer_row + 1) == framebuffer_rows)
                {
                        scroll_up();
                }
                else
                {
                        framebuffer_row++;
                }
        }
}

// To be called only after the framebuffer has been initialized.

static void framebuffer_set_color(uint32_t background_color, uint32_t foreground_color)
{
        framebuffer_background_color = ((background_color >> 16) << framebuffer_info.framebuffer_red_position) | (((background_color >> 8) & 0xFF) << framebuffer_info.framebuffer_green_position) | ((background_color & 0xFF) << framebuffer_info.framebuffer_blue_position);
        framebuffer_foreground_color = ((foreground_color >> 16) << framebuffer_info.framebuffer_red_position) | (((foreground_color >> 8) & 0xFF) << framebuffer_info.framebuffer_green_position) | ((foreground_color & 0xFF) << framebuffer_info.framebuffer_blue_position);
}

void bootconsole_framebuffer_put_char(char c)
{
        if (c == '\n')
        {
                framebuffer_column = 0;
                if ((size_t)(framebuffer_row + 1) == framebuffer_rows)
                {
                        scroll_up();
                }
                else
                {
                        framebuffer_row++;
                }
        }
        else if (c == '\t')
        {
                for (size_t i = 0; i < TAB_SIZE; i++)
                {
                        framebuffer_put_char(' ', framebuffer_row, framebuffer_column);
                        advance_and_scroll_if_needed();
                }
        }
        else
        {
                framebuffer_put_char(c, framebuffer_row, framebuffer_column);
                advance_and_scroll_if_needed();
        }
}

void bootconsole_framebuffer_put_string(const char *s, size_t size)
{
        for (size_t i = 0; i < size; i++)
        {
                bootconsole_framebuffer_put_char(s[i]);
        }
}

bootconsole_t bootconsole_framebuffer = {
    .console_init = bootconsole_framebuffer_init,
    .console_put_char = bootconsole_framebuffer_put_char,
    .console_put_string = bootconsole_framebuffer_put_string};