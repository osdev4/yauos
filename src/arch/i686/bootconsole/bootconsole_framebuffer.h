#ifndef _BOOTCONSOLE_FRAMEBUFFER_H
#define _BOOTCONSOLE_FRAMEBUFFER_H

#include <stddef.h>
#include <stdint.h>
#include <limits.h>
#include <arch/types.h>

#define PSF_MAGIC 0x864AB572
#define PSF_FLAG_UNICODE_PRESENT 1
#define PIXEL uint32_t
#define TAB_SIZE 4

extern uintptr_t _binary_console_font_start;
extern uintptr_t _binary_console_font_end;

typedef struct bootconsole_framebuffer_data
{
        phys_addr_t framebuffer_phys_address;
        virt_addr_t framebuffer_virt_address;
        uint32_t framebuffer_width;
        uint32_t framebuffer_height;
        uint32_t framebuffer_pitch;
        uint8_t framebuffer_bpp;
        uint8_t framebuffer_red_position;
        uint8_t framebuffer_red_mask;
        uint8_t framebuffer_green_position;
        uint8_t framebuffer_green_mask;
        uint8_t framebuffer_blue_position;
        uint8_t framebuffer_blue_mask;

} bootconsole_framebuffer_data_t;

typedef struct psf_font
{
        uint32_t magic;
        uint32_t version;
        uint32_t header_size;
        uint32_t flags;
        uint32_t number_of_glyphs;
        uint32_t bytes_per_glyph;
        uint32_t height;
        uint32_t width;
} psf_font_t;

// Color definitions in RGB format independent of the underlying framebuffer implementation (RRGGBB).

#define BLACK 0x000000
#define BLUE 0x0000FF
#define GREEN 0x008000
#define ORANGE 0xFFA500
#define RED 0xFF0000
#define VIOLET 0xEE82EE
#define WHITE 0xFFFFFF
#define YELLOW 0xFFFF00

static void psf_unicode_table_init(void);
static void scroll_up(void);
static void advance_and_scroll_if_needed(void);
static void framebuffer_put_char(uint16_t, uint32_t, uint32_t);
static void framebuffer_set_color(uint32_t, uint32_t);

#endif /** _BOOTCONSOLE_FRAMEBUFFER_H */