#include <stdbool.h>
#include <stddef.h>
#include <kernel/bootconsole.h>

spinlock_t bootconsole_lock = {
    .name = "bootconsole",
    .cpu_id = 0xFF,
    .locking = false,
    .locked = false,
    .nested = 0,
    .lock = 0};

static bootconsole_t *bootconsole;
static bool bootconsole_enabled = 1;

void bootconsole_disable()
{
        bootconsole_enabled = 0;
}

bool bootconsole_is_enabled()
{
        return bootconsole_enabled;
}

int bootconsole_init(bootconsole_type_t console_type, __attribute__((unused))  void *data)
{
        switch (console_type)
        {
        case BOOTCONSOLE_MEM:
                bootconsole = &bootconsole_mem;
                return bootconsole->console_init();
        case BOOTCONSOLE_SERIAL:
                bootconsole = &bootconsole_serial;
                return bootconsole->console_init();
        case BOOTCONSOLE_VGA_TEXT_MODE:
                bootconsole = &bootconsole_vga_text_mode;
                return bootconsole->console_init();
        case BOOTCONSOLE_FRAMEBUFFER:
		#ifdef FRAMEBUFFER
                
			bootconsole = &bootconsole_framebuffer;
                	return bootconsole->console_init(data);
		
		#endif /* FRAMEBUFFER */
        default:
                bootconsole = &bootconsole_serial;
                return bootconsole->console_init();
        };
}

void bootconsole_put_char(char c)
{
        bootconsole->console_put_char(c);
}

void bootconsole_put_string(const char *s, size_t size)
{
        bootconsole->console_put_string(s, size);
}
