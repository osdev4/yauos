#include <cpuid.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <arch/align.h>
#include <arch/cpu/cpu.h>
#include <arch/cpu/gdt.h>
#include <arch/cpu/idt.h>
#ifdef SMP
#include <arch/cpu/lapic.h>
#include <arch/cpu/smp.h>
#endif
#include <arch/kernel/mm/vm.h>
#include <arch/paging.h>
#include <arch/types.h>
#include <kernel/bootconsole.h>
#include <kernel/assert.h>
#include <kernel/bootinfo.h>
#include <kernel/bootmem.h>
#include <kernel/common.h>
#include <kernel/interrupt.h>
#include <kernel/printk.h>
#include <kernel/mm/pm.h>
#include <lib/string.h>
#include <platform/multiboot2.h>
#include <platform/pic.h>
#include <platform/pit.h>

uintptr_t __stack_chk_guard = 0xDEADBEEF;

void __stack_chk_fail(void)
{
        panic("[KERNEL]: CPU[%d]: Stack corruption detected!\n", cpu->cpu_id);
}

void panic_ipi(void)
{
        lock(&bootconsole_lock);
        printk("CPU[%d] halting execution!\n", cpu->cpu_id);
        unlock(&bootconsole_lock);
        while (1)
        {
                arch_halt();
        }
}

/*
 * The code below is used to set correctly per cpu data structures in both smp and non smp cases with a unified interface.
 * To understand this you can have a look in arch/i386/cpu/smp.c arch/i386/cpu/gdt/gdt.c
 * Per cpu variables and data structures are made possible through the use of the GS segment. In both smp and non smp cases the gdt initialization code manipulates a cpu_data_t structure (which is slightly different in the 2 cases). If smp is defined the code in arch/i386/cpu/smp.c allocates an array of cpu_data_t strucutres equal to the number of cpus. In the latter case (non smp) we allocate a single cpu_data_t structure below and declare a pointer "cpu" to it. "cpu" is the pointer that the kernel expects to use for cpu specific information in both smp and non smp cases.
 */

#ifndef SMP
cpu_data_t _cpu_data;
#endif
#ifdef SMP
cpu_data_t *cpu_data;
extern uintptr_t _binary_boot_ap_start;
extern uintptr_t _binary_boot_ap_size;
#else
cpu_data_t *cpu_data = &_cpu_data;
cpu_data_t *cpu = &_cpu_data;
#endif
extern size_t bootconsole_mem_get_number_buffered_items();
extern void bootconsole_mem_flush_buffer(char *);

// Defined in kernel/pm.c

extern void pmm_init(bootinfo_t *);

// Arch independent entry point.

extern void kernel_main();

bootinfo_t *boot_info;
bool arch_init = true;
#ifdef SMP
bool volatile ap_started = false;
#endif

static void parse_cmdline(multiboot2_information_header_t *m_boot2_info)
{
        boot_info->karg_entries = 0;
        boot_info->karg_entry = NULL;
        multiboot2_tag_header_t *tag;
        for (tag = (multiboot2_tag_header_t *)((uintptr_t)(m_boot2_info) + 8); tag->type != MULTIBOOT2_TAG_END_TYPE;)
        {

                // TODO: Better handle corner cases for the kernel command line errors.

                if (tag->type == MULTIBOOT2_TAG_BOOT_COMMAND_LINE_TYPE)
                {
                        multiboot2_tag_boot_command_line_t *cmd_line = (multiboot2_tag_boot_command_line_t *)tag;
                        char *saved_cmd_line = (char *)b_malloc(sizeof(char) * cmd_line->size);
                        if (unlikely(!saved_cmd_line))
                        {
                                panic("[KERNEL]: Failed to allocate memory! File: %s line: %d function: %s\n", __FILENAME__, __LINE__, __func__);
                        }
                        memcpy(saved_cmd_line, cmd_line->string, cmd_line->size);
                        boot_info->command_line = saved_cmd_line;
                        size_t n_args = 0;

                        // 9 is the size in bytes of an empty command line. See multiboot2.h file.

                        if (cmd_line->size == 9)
                        {
                                break;
                        }
                        for (size_t i = 0; i < cmd_line->size - 8; i++)
                        {

                                // Calculate how much to allocate for the kernel arguments. Args are separated by a space or null character.

                                if (cmd_line->string[i] == ' ' || cmd_line->string[i] == '\0')
                                {
                                        n_args++;
                                }
                        }
                        boot_info->karg_entries = n_args;
                        karg_t *kargs = (karg_t *)b_malloc(sizeof(karg_t) * n_args);
                        if (unlikely(!kargs))
                        {
                                panic("[KERNEL]: Failed to allocate memory! File: %s line: %d function: %s\n", __FILENAME__, __LINE__, __func__);
                        }

                        // Set all entries as NULL initially.

                        for (size_t i = 0; i < n_args; i++)
                        {
                                kargs[i].key = NULL;
                                kargs[i].value = NULL;
                        }

                        /*
                         * While looping over the command line string, is_key is 1 until a key is succesfully parsed, then it is set to 0 and
                         * a value is parsed next. This goes on until a value is being parsed and the NULL character is found.
                         */

                        bool is_key = 1;
                        size_t key_size = 0;
                        size_t value_size = 0;
                        for (size_t i = 0, j = 0; i < cmd_line->size - 8; i++)
                        {
                                if (is_key)
                                {
                                        key_size++;
                                }
                                else
                                {
                                        value_size++;
                                }

                                // = is the ending character for a key because we support key value pairs in the format KEY=VAL.

                                if (is_key && (cmd_line->string[i] == '='))
                                {
                                        char *key = (char *)b_malloc(sizeof(char) * key_size);
                                        if (unlikely(!key))
                                        {
                                                panic("[KERNEL]: Failed to allocate memory! File: %s line: %d function: %s\n", __FILENAME__, __LINE__, __func__);
                                        }

                                        /*
                                         * key_size takes into account the = character for its size calculation, thus the key is copied
                                         * until key_size - 1 to leave space for the NULL character which replaces the = character.
                                         */

                                        memcpy(key, &cmd_line->string[i - (key_size - 1)], key_size - 1);
                                        key[key_size] = '\0';

                                        // Check if this key already exists and panic in case it does. Keys should be unique.

                                        for (size_t i = 0; i < n_args; i++)
                                        {
                                                if (kargs[i].key != NULL)
                                                {
                                                        if (unlikely(strcmp(kargs[i].key, key) == 0))
                                                        {
                                                                panic("[KERNEL]: Command line has multiple definitions for the same key! File: %s line: %d function: %s\n", __FILENAME__, __LINE__, __func__);
                                                                break;
                                                        }
                                                }
                                        }
                                        kargs[j].key = key;

                                        // Reset key size for the next parse

                                        key_size = 0;

                                        // Indicates the start of a value parse.

                                        is_key = 0;
                                }

                                // The same as above but for a value. We expect a space or a NULL character after a key.

                                if (!is_key && (cmd_line->string[i] == ' ' || cmd_line->string[i] == '\0'))
                                {
                                        char *value = (char *)b_malloc(sizeof(char) * value_size);
                                        if (unlikely(!value))
                                        {
                                                panic("[KERNEL]: Failed to allocate memory! File: %s line: %d function: %s\n", __FILENAME__, __LINE__, __func__);
                                        }
                                        memcpy(value, &cmd_line->string[i - (value_size - 1)], value_size - 1);
                                        value[value_size] = '\0';
                                        kargs[j].value = value;
                                        j++;

                                        // Reset value size for the next parse.

                                        value_size = 0;

                                        // Indicates a new key parse.

                                        is_key = 1;

                                        // If a NULL character is found while parsing a value it means we are at the end of the command line.

                                        if (cmd_line->string[i] == '\0')
                                        {
                                                break;
                                        }
                                }
                        }

                        // Set the parsed entries in the bootinfo structure.

                        boot_info->karg_entry = kargs;
                        break;
                }

                // Multiboot2 tags are 8 byte aligned. See multiboo2.h for more info.

                tag = (multiboot2_tag_header_t *)ALIGN(((uintptr_t)tag + tag->size), 8);
        }
}

static int parse_symbol_table(multiboot2_information_header_t *m_boot2_info)
{
        multiboot2_tag_header_t *tag;
        for (tag = (multiboot2_tag_header_t *)((uintptr_t)(m_boot2_info) + 8); tag->type != MULTIBOOT2_TAG_END_TYPE;)
        {
                if (tag->type == MULTIBOOT2_TAG_MODULES_TYPE)
                {
                        multiboot2_tag_modules_t *module = (multiboot2_tag_modules_t *)tag;
                        if (strcmp((char *)module->string, "symbol_table") != 0)
                        {
                                continue;
                        }
                        uint32_t n_symbols = 0;
                        for (char *p = (char *)PHYSICAL_TO_VIRTUAL(module->mod_start); p != (char *)PHYSICAL_TO_VIRTUAL(module->mod_end); p++)
                        {
                                if (*p == '\n')
                                {
                                        n_symbols++;
                                }
                        }
                        symbol_table_t *symtab = (symbol_table_t *)b_malloc(sizeof(symbol_table_t) * n_symbols);
                        if (unlikely(!symtab))
                        {
                                panic("[KERNEL]: Failed to allocate memory! File: %s line: %d function: %s\n", __FILENAME__, __LINE__, __func__);
                        }
                        boot_info->symbol_table_entries = n_symbols;
                        boot_info->symbol_table = symtab;
                        size_t cur_symbol = 0;
                        size_t cur_size = 0;
                        for (char *p = (char *)PHYSICAL_TO_VIRTUAL(module->mod_start); p < (char *)PHYSICAL_TO_VIRTUAL(module->mod_end); p++)
                        {
                                if (*p == '=')
                                {
                                        char address[cur_size + 1];
                                        memset(address, 0, cur_size + 1);
                                        memcpy(address, (char *)(p - cur_size), cur_size);
                                        address[cur_size + 1] = '\0';
                                        symtab[cur_symbol].address = atoi(address, 16);
                                        cur_size = 0;
                                        continue;
                                }
                                else if (*p == '\n')
                                {
                                        char *function_name = (char *)b_malloc(sizeof(char) * (cur_size + 1));
                                        if (unlikely(!function_name))
                                        {
                                                panic("[KERNEL]: Failed to allocate memory! File: %s line: %d function: %s\n", __FILENAME__, __LINE__, __func__);
                                        }
                                        memcpy(function_name, (char *)(p - cur_size), cur_size);
                                        function_name[cur_size + 1] = '\0';
                                        symtab[cur_symbol].function_name = function_name;
                                        cur_symbol++;
                                        cur_size = 0;
                                        continue;
                                }
                                cur_size++;
                        }
                        return 0;
                }

                // Multiboot2 tags are 8 byte aligned. See multiboo2.h for more info.

                tag = (multiboot2_tag_header_t *)ALIGN(((uintptr_t)tag + tag->size), 8);
        }
        return -1;
}

static void parse_memory_map(multiboot2_information_header_t *m_boot2_info)
{
        multiboot2_tag_header_t *tag;

        /*
         * Copy the memory map provided by the bootloader to the kernel bootinfo structure.
         * Note: Since this is a hobby educational project mainly used to learn by doing
         * not all possible scenarios have been covered and some assumptions that simplify coding have been made.
         * One of these assumptions is that the firmware provides a sane memory map.
         * This code has been run mainly on Qemu and it's memory map looks sane enough.
         * This code has also been tested this on some real machines (mainly 64 bit systems with UEFI-CSM) and the memory
         * maps there have also been confirmed to be sane enough.
         */

        for (tag = (multiboot2_tag_header_t *)((uintptr_t)(m_boot2_info) + 8); tag->type != MULTIBOOT2_TAG_END_TYPE;)
        {
                if (tag->type == MULTIBOOT2_TAG_MEMORY_MAP_TYPE)
                {
                        multiboot2_tag_memory_map_t *memory_map = (multiboot2_tag_memory_map_t *)tag;
                        multiboot2_tag_memory_map_entry_t *entry = (multiboot2_tag_memory_map_entry_t *)memory_map->entries;
                        size_t number_entries = (memory_map->size - 16) / memory_map->entry_size;

                        /*
                         * This kernel does not support PAE so we skip entries above 0xFFFFFFFF. If we add the 3Gb memory hole
                         * this means that on this architecture the maximum usable ram is limited to 3Gb because any ram pushed
                         * above 0xFFFFFFFF is not addressable.
                         * The calculation of the index below is done to allocate only the number of entries we want.
                         */

                        size_t number_entries_final = 0;
                        for (size_t i = 0; i < number_entries; i++)
                        {
                                if (entry[i].base_addr > 0xFFFFFFFF)
                                {
                                        break;
                                }
                                number_entries_final++;
                        }
                        boot_info->memory_map_entries = number_entries_final;
                        memory_entry_t *memory = (memory_entry_t *)b_malloc(sizeof(memory_entry_t) * number_entries_final);
                        if (unlikely(!memory))
                        {
                                panic("Failed to allocate memory! File: %s line: %d function: %s\n", __FILENAME__, __LINE__, __func__);
                        }

                        // Copy only the entries below 0xFFFFFFFF.

                        for (size_t i = 0; i < number_entries; i++)
                        {

                                /*
                                 * This is because we don't know if entries in the map are in sorted order and to
                                 * display the user all available memory even if it cannot be used. (usually they are but cannot be sure about it).
                                 */

                                if (entry[i].base_addr > 0xFFFFFFFF)
                                {

                                        // This is just to track all the available memory (even if it is not usable by this kernel, for statistical purposes).

                                        if (entry[i].type == MULTIBOOT2_MEMORY_AVAILABLE || entry[i].type == MULTIBOOT2_MEMORY_ACPI_RECLAIMABLE)
                                        {
                                                boot_info->memory_size += entry[i].length;
                                        }
                                        continue;
                                }
                                if (entry[i].type == MULTIBOOT2_MEMORY_AVAILABLE)
                                {
                                        boot_info->memory_size += entry[i].length;
                                        memory[i].type = MEMORY_AVAILABLE;
                                }
                                else if (entry[i].type == MULTIBOOT2_MEMORY_ACPI_RECLAIMABLE)
                                {
                                        boot_info->memory_size += entry[i].length;
                                        memory[i].type = MEMORY_RECLAIMABLE;
                                }
                                else
                                {
                                        memory[i].type = MEMORY_RESERVED;
                                }
                                memory[i].base_addr = entry[i].base_addr;
                                memory[i].length = entry[i].length;
                        }
                        boot_info->memory_map_entry = memory;
                        break;
                }
                tag = (multiboot2_tag_header_t *)ALIGN(((uintptr_t)(tag) + tag->size), 8);
        }
}

/*
 * This routine parses the kernel command line that has been made for the upper level kernel to check if
 * any bootconsole=xxx parameter has been specified in the command line and tries to initialize that bootconsole.
 * If nothing is specified the kernel fallbacks to the serial implementation and if that fails too it fallbacks to
 * the vga implementation. The code assumes the presence of the standard vga text mode on the target platform (i386-pc).
 * Note: the makefile by default is set to use serial bootconsole. If you try to run on real hardware remember to set
 * that parameter to vga or ensure to have a serial connection working.
 */

static int boot_console_init(multiboot2_information_header_t *m_boot2_info)
{
        if (boot_info->karg_entries != 0)
        {
                for (size_t i = 0; i < boot_info->karg_entries; i++)
                {
                        if (boot_info->karg_entry[i].key != NULL && strcmp(boot_info->karg_entry[i].key, "bootconsole") == 0)
                        {

                                // Serial bootconsole was specified on the command line.

                                if (boot_info->karg_entry[i].value != NULL && strcmp(boot_info->karg_entry[i].value, "serial") == 0)
                                {
                                        printk("[KERNEL]: Switching boot console.\n[KERNEL]: Initializing serial boot console.\n");
                                        char *buf = (char *)b_malloc(sizeof(char) * bootconsole_mem_get_number_buffered_items());
                                        if (unlikely(!buf))
                                        {
                                                panic("[KERNEL]: Failed to allocate memory! File: %s line: %d function: %s\n", __FILENAME__, __LINE__, __func__);
                                        }
                                        bootconsole_mem_flush_buffer(buf);
                                        if (unlikely(bootconsole_init(BOOTCONSOLE_SERIAL)))
                                        {
                                                b_free(buf);

                                                // Serial bootconsole failed, switch back to memory ringbuffer to not waste output during switch to vga implementation.

                                                if (unlikely(bootconsole_init(BOOTCONSOLE_MEM)))
                                                {
                                                        return -1;
                                                }
                                                printk("[KERNEL]: Failed to initialize serial bootconsole, falling back to vga text mode.\n[KERNEL]: initializing vga text mode boot console.\n");
                                                buf = (char *)b_malloc(sizeof(char) * bootconsole_mem_get_number_buffered_items());
                                                if (unlikely(!buf))
                                                {
                                                        panic("[KERNEL]: Failed to allocate memory! File: %s line: %d function: %s\n", __FILENAME__, __LINE__, __func__);
                                                }

                                                // Finally switch back to vga bootconsole.

                                                bootconsole_mem_flush_buffer(buf);
                                                if (unlikely(bootconsole_init(BOOTCONSOLE_VGA_TEXT_MODE)))
                                                {
                                                        b_free(buf);
                                                        return -1;
                                                }
                                                bootconsole_put_string(buf, bootconsole_mem_get_number_buffered_items());
                                                printk("[KERNEL]: Initialized vga text mode boot console.\n");
                                                return 0;
                                        }
                                        else
                                        {
                                                bootconsole_put_string(buf, bootconsole_mem_get_number_buffered_items());
                                                printk("[KERNEL]: Initialized serial boot console.\n");
                                                return 0;
                                        }
                                }

                                // Vga bootconsole was specified on the command line.

                                else if (boot_info->karg_entry[i].value != NULL && strcmp(boot_info->karg_entry[i].value, "vga") == 0)
                                {
                                        printk("[KERNEL]: Switching boot console.\n[KERNEL]: Initializing vga text mode boot console.\n");
                                        char *buf = (char *)b_malloc(sizeof(char) * bootconsole_mem_get_number_buffered_items());
                                        if (unlikely(!buf))
                                        {
                                                panic("[KERNEL]: Failed to allocated memory! File: %s line: %d function: %s\n", __FILENAME__, __LINE__, __func__);
                                        }
                                        bootconsole_mem_flush_buffer(buf);
                                        if (unlikely(bootconsole_init(BOOTCONSOLE_VGA_TEXT_MODE)))
                                        {
                                                b_free(buf);
                                                return -1;
                                        }
                                        bootconsole_put_string(buf, bootconsole_mem_get_number_buffered_items());
                                        printk("[KERNEL]: Initialized vga text mode boot console.\n");
                                        return 0;
                                }

                                // Framebuffer bootconsole was specified on the command line.

                                else if (boot_info->karg_entry[i].value != NULL && strcmp(boot_info->karg_entry[i].value, "framebuffer") == 0)
                                {
                                        printk("[KERNEL]: Switching boot console.\n[KERNEL]: Initializing framebuffer boot console.\n");
                                        char *buf = (char *)b_malloc(sizeof(char) * bootconsole_mem_get_number_buffered_items());
                                        if (unlikely(!buf))
                                        {
                                                panic("[KERNEL]: Failed to allocate memory! File: %s line: %d function: %s\n", __FILENAME__, __LINE__, __func__);
                                        }
                                        bootconsole_mem_flush_buffer(buf);
                                        if (unlikely(bootconsole_init(BOOTCONSOLE_FRAMEBUFFER, m_boot2_info)))
                                        {
                                                b_free(buf);

                                                // Framebuffer bootconsole failed, switch back to memory ringbuffer to not waste output during switch to vga implementation.

                                                if (unlikely(bootconsole_init(BOOTCONSOLE_MEM)))
                                                {
                                                        return -1;
                                                }
                                                printk("[KERNEL]: Failed to initialize framebuffer bootconsole, falling back to vga text mode.\n[KERNEL]: initializing vga text mode boot console.\n");
                                                buf = (char *)b_malloc(sizeof(char) * bootconsole_mem_get_number_buffered_items());
                                                if (unlikely(!buf))
                                                {
                                                        panic("[KERNEL]: Failed to allocate memory! File: %s line: %d function: %s\n", __FILENAME__, __LINE__, __func__);
                                                }

                                                // Finally switch back to vga bootconsole.

                                                bootconsole_mem_flush_buffer(buf);
                                                if (unlikely(bootconsole_init(BOOTCONSOLE_VGA_TEXT_MODE)))
                                                {
                                                        b_free(buf);
                                                        return -1;
                                                }
                                                bootconsole_put_string(buf, bootconsole_mem_get_number_buffered_items());
                                                printk("[KERNEL]: Initialized vga text mode boot console.\n");
                                                return 0;
                                        }
                                        else
                                        {
                                                bootconsole_put_string(buf, bootconsole_mem_get_number_buffered_items());
                                                printk("[KERNEL]: Initialized framebuffer boot console.\n");
                                                return 0;
                                        }
                                }

                                // Empty bootconsole value was specified, default to vga.

                                else
                                {
                                        printk("[KERNEL]: No boot console specified in command line, defaulting to vga boot console.\n[KERNEL]: Initializing serial boot console.\n");
                                        char *buf = (char *)b_malloc(sizeof(char) * bootconsole_mem_get_number_buffered_items());
                                        if (unlikely(!buf))
                                        {
                                                panic("[KERNEL]: Failed to allocated memory! File: %s line: %d function: %s\n", __FILENAME__, __LINE__, __func__);
                                        }
                                        bootconsole_mem_flush_buffer(buf);
                                        if (unlikely(bootconsole_init(BOOTCONSOLE_VGA_TEXT_MODE)))
                                        {
                                                b_free(buf);
                                                return -1;
                                        }
                                        bootconsole_put_string(buf, bootconsole_mem_get_number_buffered_items());
                                        printk("[KERNEL]: Initialized vga boot console.\n");
                                        return 0;
                                }
                        }

                        // No bootconsole key was found on the command line.

                        else
                        {
                                printk("[KERNEL]: No parameters specified in kernel command line, defaulting to vga boot console.\n[KERNEL]: Initializing vga boot console.\n");
                                char *buf = (char *)b_malloc(sizeof(char) * bootconsole_mem_get_number_buffered_items());
                                if (unlikely(!buf))
                                {
                                        panic("[KERNEL]: Failed to allocated memory! File: %s line: %d function: %s\n", __FILENAME__, __LINE__, __func__);
                                }
                                bootconsole_mem_flush_buffer(buf);
                                if (unlikely(bootconsole_init(BOOTCONSOLE_VGA_TEXT_MODE)))
                                {
                                        b_free(buf);
                                        return -1;
                                }
                                bootconsole_put_string(buf, bootconsole_mem_get_number_buffered_items());
                                printk("[KERNEL]: Initialized vga boot console.\n");
                                return 0;
                        }
                }
        }

        // Command line is empty, default to serial bootconsole.

        else
        {
                printk("[KERNEL]: No parameters specified in kernel command line, defaulting to vga boot console.\n[KERNEL]: Initializing vga boot console.\n");
                char *buf = (char *)b_malloc(sizeof(char) * bootconsole_mem_get_number_buffered_items());
                if (unlikely(!buf))
                {
                        panic("[KERNEL]: Failed to allocated memory! File: %s line: %d function: %s\n", __FILENAME__, __LINE__, __func__);
                }
                bootconsole_mem_flush_buffer(buf);
                if (unlikely(bootconsole_init(BOOTCONSOLE_VGA_TEXT_MODE)))
                {
                        b_free(buf);
                        return -1;
                }
                bootconsole_put_string(buf, bootconsole_mem_get_number_buffered_items());
                printk("[KERNEL]: Initialized vga boot console.\n");
                return 0;
        }
        return -1;
}

static int check_cpuid()
{
        return __get_cpuid_max(0, NULL);
}

static int check_sse()
{
        unsigned int unused, edx = 0;
        __get_cpuid(1, &unused, &unused, &unused, &edx);

        // Here we check for both SSE and SSE2

        if ((edx & (1 << 26)) && (edx & (1 << 27)))
        {
                return 0;
        }
        return -1;
}

static int check_fxsave_fxrstr()
{
        unsigned unused, edx = 0;
        __get_cpuid(1, &unused, &unused, &unused, &edx);
        if (edx & (1 << 24))
        {
                return 0;
        }
        return -1;
}

static int check_fpu()
{
        unsigned unused, edx = 0;
        __get_cpuid(1, &unused, &unused, &unused, &edx);
        if (edx & 0x1)
        {
                return 0;
        }
        return -1;
}

static void init_fpu()
{
        uint32_t cr0 = read_cr0();

        // Enable FPU exceptions and enable interaction with the TS bit in cr0 upon task switches to save/load fpu context.

        cr0 |= (1 << CR0_MONITOR_CO_PROCESSOR_SHIFT) | (1 << CR0_X87_FPU_ERROR_REPORTING_SHIFT);

        // Disable FPU emulation exceptions because we have onboard FPU (and only support that).

        cr0 &= ~(1 << CR0_X87_FPU_SHIFT);
        write_cr0(cr0);
        __asm volatile("fninit\n");
}

static void init_sse()
{
        uint32_t cr4 = read_cr4();
        cr4 |= (1 << 9) | (1 << 10);
        write_cr4(cr4);
}

#ifdef SMP

/*
 * This is used to count milliseconds elapsed since the counter began counting.
 */

static volatile uint64_t elapsed_milliseconds = 0;

/*
 * This is used for the delays required for the smp ap startup code.
 */

static void timer_callback()
{
        elapsed_milliseconds++;
}

/*
 * This is used for delaying for a specified amount of milliseconds.
 */

void delay(uint64_t ms)
{
        uint64_t end = elapsed_milliseconds + ms;
        while (elapsed_milliseconds < end)
        {
                arch_halt();
        }
}

uint32_t volatile ap_done_with_gdt = 1;
bool volatile barrier = false;
uint32_t ap_can_go = 0;
static uint64_t volatile ms[4] = {0, 0, 0, 0};

void lapic_timer(void)
{
        ms[cpu->cpu_id] += 10;
}

void delay_l(uint64_t milliseconds)
{
        uint64_t end = ms[cpu->cpu_id] + milliseconds;
        while (ms[cpu->cpu_id] < end)
        {
                arch_halt();
        }
}

uint32_t volatile num_started = 1;
static uint32_t ticksin10ms = 0;

void lapic_calibrate()
{
        lapic_enable_timer(0);
        delay(10);
        lapic_disable_timer();
        ticksin10ms = 0xFFFFFFFF - lapic_get_current_count();
        lock(&bootconsole_lock);
        printk("CPU: %x Apic timer frequency: %dMhz\n", cpu->cpu_id, (uint32_t)(((float)ticksin10ms / (10.0 / 1000.0)) / 1000000.0));
        unlock(&bootconsole_lock);
        lapic_enable_timer(ticksin10ms);
}

void smp_main(uint8_t lapic_id)
{
        num_started++;
        ap_started = true;
        while (_xchg(1, &ap_can_go) != 0)
                ;

        /*
         * gdt_init() calls b_malloc(), but we have set locks active now so care must be taken
         * to let each cpu initialize its gdt serially. This is so because the spinlocks use per cpu
         * data structures that are not initialized before gdt_init() and accessing them inside
         * spinlocks before gdt and idt are setup results in a crash without any debug information.
         */

        gdt_init(lapic_id);
        ap_done_with_gdt++;
        _xchg(0, &ap_can_go);
        while (!barrier)
                ;
        idt_init(false);
        init_fpu();
        init_sse();
        lock(&bootconsole_lock);
        printk("AP[%x]: started and initialized!\nAP[%x]: gdt address: %x\nper cpu structure address: %x\n", cpu->cpu_id, cpu->cpu_id, cpu->gdt, cpu);
        unlock(&bootconsole_lock);
        lapic_init();
        lapic_enable_timer(ticksin10ms);
        _arch_enable_interrupts();
        while (1)
        {
                delay_l(1000);
                lock(&bootconsole_lock);
                printk("CPU (%d): 1 second has elapsed!\n", cpu->cpu_id);
                unlock(&bootconsole_lock);
        }
}
#endif

/**
 * This is the kernel architecture specific entry point after the early boot code. Here the first steps of initialization are done: architecture specific initialization (GDT, IDT, etc...), reading the memory map and formatting it in the kernel expected way, initialization of the early boot console etc.
 */

void arch_main(uint32_t magic, multiboot2_information_header_t *m_boot2_info)
{
        /*
         * Setup initial bootconsole device to the memory ringbuffer for early output.
         * The bootconsole is the first thing initialized after gdt and idt to have significant output in case of boot failure or in case
         * of in-kernel bugs (exceptions). Only after the multiboot2 magic code is checked the rest of the initialization can proceed.
         */

        if (unlikely(bootconsole_init(BOOTCONSOLE_MEM)))
        {
                panic("[KERNEL]: Failed to initialize bootconsole!\n");
        }
        printk("[KERNEL]: Initialized bootconsole_mem!\n");

        /*
         * The multiboot2 strcutures and all the relative data like modules are loaded after the kernel by the bootloader and after pmm_init()
         * that data is likely to be overwritten causing all troubles.
         * For now the order of initializing things matters even more than normal due to this.
         * Other things like the framebuffer initialization rely on mapping pages and so rely on the physical memory manager being up.
         * This why the multiboot2 strucutres are copied elsewhere.
         */

        multiboot2_information_header_t *m_boot2_info_s = (multiboot2_information_header_t *)b_malloc(m_boot2_info->total_size);
        if (unlikely(!m_boot2_info_s))
        {
                panic("[KERNEL]: Failed to allocate memory! File: %s line: %d function: %s\n", __FILENAME__, __LINE__, __func__);
        }
        memcpy(m_boot2_info_s, m_boot2_info, m_boot2_info->total_size);
        boot_info = (bootinfo_t *)b_malloc(sizeof(bootinfo_t));
        if (unlikely(!boot_info))
        {
                panic("[KERNEL]: Failed to allocate memory! File: %s line: %d function: %s\n", __FILENAME__, __LINE__, __func__);
        }
        memset(boot_info, 0x0, sizeof(bootinfo_t));

        /*
         * Parse kernel symbol table for stack trace function names.
         * TODO: implement DWARF debugging for in-kernel use.
         */

        if (unlikely(parse_symbol_table(m_boot2_info_s)))
        {
                panic("[KERNEL]: Could not load kernel symbol table!\n");
        }

        // Parse the multiboot2 memory map and format it according to the way the upper kernel layer expects it.

        parse_memory_map(m_boot2_info_s);

        // smp_init() must come before pmm_init().
#ifdef SMP
        smp_init();
#endif
        pmm_init(boot_info);

        /*
         * TODO: move the command line parsing one layer above the architecture specific code and initialize the early console with a fixed default one.
         * Parse the kernel command line and format it for easier access to the upper kernel layer.
         */

        parse_cmdline(m_boot2_info_s);

        /*
         * Initialize the real bootconsole according to kernel command line or defaults.
         * If the bootconsole fails to initialize only the memory ringbuffer will be active with no output to anywhere.
         * The panic will halt the machine though and memory can be inspected with a debugger if emulating.
         * On real hardware no output will be visible. This is unlikely though, because the machines
         * this OS targets should have for sure the old vga mode or the serial (assuming it's connected).
         */

        if (unlikely(boot_console_init(m_boot2_info_s)))
        {
                panic("[KERNEL]: Failed to initialize bootconsole!\n");
        }
        if (!check_cpuid())
        {
                panic("[KERNEL]: This CPU does not support the cpuid instruction. This kernel needs a more recent CPU to run properly!\n");
        }
        if (check_sse())
        {
                panic("[KERNEL]: This CPU does not support the SSE/SSE2 instructions. This kernel needs a more recent CPU to run properly!\n");
        }
        if (check_fxsave_fxrstr())
        {
                panic("[KERNEL]: This CPU does not support the FXSAVE/FXRSTR instructions. This kernel needs a more recent CPU to run properly!\n");
        }
        if (check_fpu())
        {
                panic("[KERNEL]: This CPU does not have an onboard FPU. This kernel needs an onboard FPU to run properly!\n");
        }
        init_fpu();
        init_sse();

        // If the kernel wasn't loaded by a multiboot2 compliant bootloader fail as we rely on the provided memory map.

        if (unlikely(magic != MULTIBOOT2_MAGIC))
        {
                panic("[KERNEL]: This kernel must be loaded by a Multiboot2 compliant bootloader! File: %s line: %d function: %s\n", __FILENAME__, __LINE__, __func__);
        }
#ifdef SMP

        // Assuming 0 for the local apic identifier of the bootstrap processor.

        gdt_init(0);
        idt_init(true);
#else
        gdt_init();
        idt_init();
#endif
        pic_init();

#ifdef SMP
        if (register_interrupt_handler(32, timer_callback))
        {
                panic("[KERNEL]: Could not register interrupt handler! File: %s line: %d function: %s\n", __FILENAME__, __LINE__, __func__);
        }
        if (register_interrupt_handler(49, panic_ipi))
        {
                panic("[KERNEL]: Could not register interrupt handler! File: %s line: %d function: %s\n", __FILENAME__, __LINE__, __func__);
        }
        pit_interrupt_rate_generator(1000);
        pic_enable_irq_line(0);
        _arch_enable_interrupts();

        // Identity mapping local apic and io apic addresses to the same virtual addresses.

        printk("local apic address: %x\n", local_apic_address);
        if (unlikely(map_page(local_apic_address, local_apic_address, PROT_PRESENT | PROT_READ_WRITE | PROT_KERN | PROT_CACHE_DISABLE, false)))
        {
                panic("[KERNEL]: Could not map local apic address! File: %s line: %d function: %s\n", __FILENAME__, __LINE__, __func__);
        }
        if (unlikely(map_page(io_apic_address, io_apic_address, PROT_PRESENT | PROT_READ_WRITE | PROT_KERN | PROT_CACHE_DISABLE, false)))
        {
                panic("[KERNEL]: Could not map io apic address! File: %s line: %d function: %s\n", __FILENAME__, __LINE__, __func__);
        }
        if (register_interrupt_handler(48, lapic_timer))
        {
                lock(&bootconsole_lock);
                printk("CPU[%x]: Failed to register interrupt handler!\n", cpu->cpu_id);
                unlock(&bootconsole_lock);
        }

        /*
         * This messy shit was just for testing smp booting...now that it works it's time to organize things properly.
         * First i need to implement a virtual memory address space allocator for in kernel use because i cannot
         * randomly map physical addresses and remember them mentally.
         */

        if (smp)
        {
                printk("[KERNEL]: System is MP compliant!\nFound %d cpus!\n", num_cpus);
                for (size_t i = 0; i < num_cpus; i++)
                {
                        printk("CPU [%s] with ID: %x\n", cpu_data[i].bsp ? "BSP" : "AP", cpu_data[i].cpu_id);
                }
                printk("[KERNEL]: local apic physical address (identity mapped) on each cpu is: %x\n[KERNEL]: io apic physical address (identity mapped) is: %x\n[KERNEL]: Starting application processors...\n", io_apic_address, local_apic_address);
                lapic_init();
                lapic_calibrate();
                lapic_enable_timer(ticksin10ms);
                pic_disable_irq_line(0);
                void *dest = (void *)PHYSICAL_TO_VIRTUAL(0x1000);
                memcpy(dest, &_binary_boot_ap_start, (size_t)&_binary_boot_ap_size);
                virt_addr_t ap_stack_virtual = 0xD0000000;
                for (size_t i = 1; i < num_cpus; i++)
                {
                        void *ap_code = (void *)PHYSICAL_TO_VIRTUAL(0x1000);
                        phys_addr_t ap_stack = get_free_frame();
                        if (unlikely(ap_stack == (phys_addr_t)-1))
                        {
                                panic("[KERNEL]: Failed to allocated memory! File: %s line: %d function: %s\n", __FILENAME__, __LINE__, __func__);
                        }

                        // Map ap cpus stack to virtual address starting at 0xD0000000 in the kernel directory.

                        if (map_page(ap_stack, ap_stack_virtual, PROT_PRESENT | PROT_READ_WRITE | PROT_KERN, false))
                        {
                                panic("[KERNEL]: Could not map stack for CPU %x! File: %s line: %d function: %s\n", cpu_data[i].cpu_id, __FILENAME__, __LINE__, __func__);
                        }
                        *(void **)((uintptr_t)ap_code - 4) = (void *)VIRTUAL_TO_PHYSICAL(&kernel_directory);
                        *(void **)((uintptr_t)ap_code - 8) = (void *)((uintptr_t)ap_stack_virtual + 4096);
                        *(void **)((uintptr_t)ap_code - 12) = (void *)(uint32_t)smp_main;
                        *(void **)((uintptr_t)ap_code - 16) = (void *)(uint32_t)cpu_data[i].cpu_id;
                        lapic_send_ipi(cpu_data[i].cpu_id, LAPIC_ICR_DELIVERY_MODE_INIT | LAPIC_ICR_DESTINATION_MODE_PHYSICAL | LAPIC_ICR_LEVEL_ASSERT | LAPIC_ICR_TRIGGER_MODE_LEVEL | LAPIC_ICR_DESTINATION_NO_SHORTHAND);
                        do
                        {
                                arch_halt();
                        } while (LAPIC_ICR_DELIVERY_STATUS(lapic_read(LAPIC_INTERRUPT_COMMAND_REGISTER_0)) != LAPIC_ICR_DELIVERY_STATUS_IDLE);
                        lapic_send_ipi(cpu_data[i].cpu_id, LAPIC_ICR_DELIVERY_MODE_INIT | LAPIC_ICR_DESTINATION_MODE_PHYSICAL | LAPIC_ICR_LEVEL_DE_ASSERT | LAPIC_ICR_TRIGGER_MODE_LEVEL | LAPIC_ICR_DESTINATION_NO_SHORTHAND);
                        do
                        {
                                arch_halt();
                        } while (LAPIC_ICR_DELIVERY_STATUS(lapic_read(LAPIC_INTERRUPT_COMMAND_REGISTER_0)) != LAPIC_ICR_DELIVERY_STATUS_IDLE);
                        delay_l(10);

                        // For now just send 1 SIPI and later add logic to send again and delay more increasingly.

                        lapic_send_ipi(cpu_data[i].cpu_id, ((uintptr_t)VIRTUAL_TO_PHYSICAL(ap_code) >> 12) | LAPIC_ICR_DELIVERY_MODE_STARTUP | LAPIC_ICR_DESTINATION_MODE_PHYSICAL | LAPIC_ICR_TRIGGER_MODE_LEVEL | LAPIC_ICR_DESTINATION_NO_SHORTHAND);

                        // Delaying more than the 200 micro seconds specified in the spec.

                        delay_l(1);
                        do
                        {
                                arch_halt();
                        } while (LAPIC_ICR_DELIVERY_STATUS(lapic_read(LAPIC_INTERRUPT_COMMAND_REGISTER_0)) != LAPIC_ICR_DELIVERY_STATUS_IDLE);
                        while (!ap_started)
                        {
                                arch_halt();
                        }
                        ap_stack_virtual += 4096;
                        ap_started = false;
                }
                while (num_started != num_cpus)
                {
                        arch_halt();
                }
                while (ap_done_with_gdt != num_cpus)
                {
                        arch_halt();
                }
                bootconsole_lock.locking = true;
                b_malloc_lock.locking = true;
                pmm_lock.locking = true;
                arch_init = false;
                barrier = true;
        }
        bootconsole_lock.locking = true;
        b_malloc_lock.locking = true;
        pmm_lock.locking = true;
        arch_init = false;
#endif
#ifndef SMP
        bootconsole_lock.locking = true;
        b_malloc_lock.locking = true;
        pmm_lock.locking = true;
        arch_init = false;
#endif
        lock(&bootconsole_lock);
        printk("BSP[%x]: gdt address: %x\nper cpu structure address: %x\n", cpu->cpu_id, cpu->gdt, cpu);
        unlock(&bootconsole_lock);
        kernel_main();
}