#ifndef _SPINLOCK_H
#define _SPINLOCK_H

#include <stdbool.h>
#include <stdint.h>
#include <arch/cpu/cpu.h>

typedef struct spinlock
{
        char name[16];
        uint8_t cpu_id;
        bool locking;
        bool locked;
        uint32_t nested;
        volatile uint32_t lock;
} spinlock_t;

void arch_lock(spinlock_t *lock);
void arch_unlock(spinlock_t *lock);
void arch_disable_interrupts(void);
void arch_enable_interrupts(void);

#endif /** _SPINLOCK_H */