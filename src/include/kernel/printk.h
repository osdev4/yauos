#ifndef PRINTK_H
#define PRINTK_H

#include <stdbool.h>
#include <kernel/assert.h>
#include <kernel/bootconsole.h>

void _printk(bool, const char *__restrict, ...);

#define printk(...) _printk(0, ##__VA_ARGS__)
#define panic(...) _printk(1, ##__VA_ARGS__)

#endif /** PRINTK_H */