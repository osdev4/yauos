#ifndef COMMOM_H
#define COMMON_H

/*
 * These are optimized versions of common operations for numbers that are powers of 2.
 * They are useful to avoid using multiplications and divisions in those parts of code that deals with numbers that are powers of 2.
 */
#define optimized_modulo_for_powers_of_2(x, n) ((x) & (n - 1))
#define optimized_log2_for_powers_of_2(x) __builtin_ctz((x))

#ifdef __GNUC__
#define likely(x) __builtin_expect((x), 1)
#define unlikely(x) __builtin_expect((x), 0)
#else
#define likely(x) (x)
#define unlikely(x) (x)
#endif

#endif /** COMMON_H */
