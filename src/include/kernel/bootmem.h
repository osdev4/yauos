#ifndef BOOTMEM_H
#define BOOTMEM_H

#include <stddef.h>
#include <stdint.h>
#include <kernel/spinlock.h>

extern spinlock_t b_malloc_lock;

void *b_malloc(size_t);
void *b_zmalloc(size_t);
void b_free(void *);
size_t b_used_memory(void);

#endif /** BOOTMEM_H */
