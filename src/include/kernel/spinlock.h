#ifndef SPINLOCK_H
#define SPINLOCK_H

#include <stdbool.h>
#include <stdint.h>
#include <arch/cpu/cpu.h>
#include <arch/kernel/spinlock.h>

void lock(spinlock_t *lock);
void unlock(spinlock_t *lock);

#endif /** SPINLOCK_H */