#ifndef BOOTCONSOLE_H
#define BOOTCONSOLE_H

#include <stdbool.h>
#include <stddef.h>
#include <arch/types.h>
#include <kernel/spinlock.h>

extern spinlock_t bootconsole_lock;

typedef enum bootconsole_type
{
        BOOTCONSOLE_MEM,
        BOOTCONSOLE_SERIAL,
        BOOTCONSOLE_VGA_TEXT_MODE,
        BOOTCONSOLE_FRAMEBUFFER
} bootconsole_type_t;

typedef struct bootconsole
{
        int (*console_init)();
        void (*console_put_char)(char);
        void (*console_put_string)(const char *, size_t);
} bootconsole_t;

extern bootconsole_t bootconsole_mem;
extern bootconsole_t bootconsole_serial;
extern bootconsole_t bootconsole_vga_text_mode;
extern bootconsole_t bootconsole_framebuffer;

// void bootconsole_disable(void);
bool bootconsole_is_enabled(void);
int bootconsole_init();
void bootconsole_put_char(char);
void bootconsole_put_string(const char *, size_t);

#endif /** BOOTCONSOLE_H */