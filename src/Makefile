# Architecture and platform settings:
ARCH?=i686
PLATFORM?=pc

USE_CCACHE:=1

ifeq ($(USE_CCACHE), 1)
	CCACHE:=ccache
else
	CCACHE:=
endif

# Toolchain settings:
COMPILER_DIR:=$(realpath ..)/toolchain/bin
COMPILER_TARGET:=$(COMPILER_DIR)/$(ARCH)-elf
AR:=$(CCACHE) $(COMPILER_TARGET)-ar
AS:=$(CCACHE) $(COMPILER_TARGET)-as
CC:=$(CCACHE) $(COMPILER_TARGET)-gcc
LD:=$(CCACHE) $(COMPILER_TARGET)-ld
NM:=$(CCACHE) $(COMPILER_TARGET)-nm
OBJCOPY:=$(CCACHE) $(COMPILER_TARGET)-objcopy
OBJDUMP:=$(CCACHE) $(COMPILER_TARGET)-objdump
READELF:=$(CCACHE) $(COMPILER_TARGET)-readelf

# Project directory settings:
PROJECT_DIR:=$(realpath ..)

# Source directories:
SRC_DIR:=$(PROJECT_DIR)/src
ARCH_DIR:=$(SRC_DIR)/arch/$(ARCH)
PLATFORM_DIR:=$(SRC_DIR)/arch/$(ARCH)/platform/$(PLATFORM)

# Header files default include directories:
INCLUDE_DIR:=$(SRC_DIR)/include
INCLUDE_ARCH_DIR:=$(INCLUDE_DIR)/arch/$(ARCH)
INCLUDE_PLATFORM_DIR:=$(INCLUDE_ARCH_DIR)/platform/$(PLATFORM)

# Misc:
LIB_DIR=$(SRC_DIR)/lib
ISO_DIR	:=$(PROJECT_DIR)/iso
OBJ_DIR:=$(PROJECT_DIR)/obj
BIN_DIR:=$(PROJECT_DIR)/bin
DOC_DIR:=$(PROJECT_DIR)/docs
LOG_DIR:=$(PROJECT_DIR)/log

# Early definition of compiler flags:
CFLAGS:=
ASFLAGS:=
LDFLAGS:=
QEMU_FLAGS:=

# Kernel compilation settings:

# Can be "vga", "framebuffer" or "serial"
# Note: if testing on real hardware remember to set it to "vga" or "framebuffer" if you don't have a serial connection setup.
# Defaults to vga if not set.
BOOTCONSOLE?=framebuffer

# Size in bytes of the early heap size (embedded in the .bss section of the kernel image).
EARLY_HEAP_SIZE?=0x40000

# Size of the kernel heap.
KERNEL_HEAP_SIZE?=0x2000000

# Enable compilation of the kernel with debugging support, assertions enabled and with debugging symbols.
DEBUG_ENABLE?=0

# This enables assertions without compiling the kernel in debug mode.
ENABLE_ASSERTIONS?=0

# Multi core support.
SMP?=1

# Qemu specific settings.
QEMU_CPU?=coreduo-v1

# Sets the number of CPUs for qemu to make available to the emulated machine.
# Has effect only if SMP is also set.
# Defaults to 2 if not set.
QEMU_NUM_CPUS?=4

# Sets the amount of RAM for to qemu to provide to the emulated machine.
# supported suffixes: K,M,G.
# Default to 128M if not set.
QEMU_MEM?=128M

# Sets the qemu architecture to emulate.
QEMU_ARCH?=i386

ifeq ($(BOOTCONSOLE),)
BOOTCONSOLE:=serial
endif

ifeq ($(BOOTCONSOLE), framebuffer)
CFLAGS+=-DFRAMEBUFFER
endif

ifeq ($(BOOTCONSOLE), serial)
QEMU_FLAGS+=-chardev stdio,id=char0,mux=on,logfile=$(LOG_DIR)/serial.log -serial chardev:char0 -mon chardev=char0 -nographic -cdrom $(ISO_DIR)/yauos.iso
else
QEMU_FLAGS+=-monitor stdio -cdrom $(ISO_DIR)/yauos.iso
endif

ifeq ($(DEBUG_ENABLE), 1)
CFLAGS+=-DDEBUG -g -O0
QEMU_FLAGS+=-S -s -d int -no-reboot -no-shutdown
else
CFLAGS+=-O3
endif

ifeq ($(ENABLE_ASSERTIONS), 1)
CFLAGS+=-DASSERT
endif

ifneq ($(QEMU_CPU),)
QEMU_FLAGS+=-cpu $(QEMU_CPU)
endif

ifeq ($(QEMU_CPU), host)
QEMU_FLAGS+=-enable-kvm
endif

ifneq ($(QEMU_MEM),)
QEMU_FLAGS+=-m $(QEMU_MEM)
else
QEMU_FLAGS+=-m 128M
endif

# Usually we would like to emulate hardware as close  as possible to real, but in qemu the legacy mp tables support is starting to lack behind as ACPI has superseeded it since many years now. Specifying threads=2 to emulate a hyperthreading enabled cpu yields wrong mp tables. The following link might help: https://lists.gnu.org/archive/html/qemu-devel/2022-01/msg04271.html
# If smp is specified but the number of cpus is not specified, default to 2 cores.
ifeq ($(SMP), 1)
ifeq ($(QEMU_NUM_CPUS),)
QEMU_NUM_CPUS:=2
endif
QEMU_FLAGS+=-smp $(QEMU_NUM_CPUS),cores=1,threads=1,sockets=$(QEMU_NUM_CPUS)
CFLAGS+=-DSMP
endif

# Source compilation settings:
CFLAGS+=-MMD -MP -I$(INCLUDE_DIR) -I$(INCLUDE_ARCH_DIR) -I$(INCLUDE_PLATFORM_DIR) -fno-omit-frame-pointer -ffreestanding -Wall -Werror -Wextra -Wpedantic -pedantic -std=c17 -DEARLY_HEAP_SIZE=$(EARLY_HEAP_SIZE) -DKERNEL_HEAP_SIZE=$(KERNEL_HEAP_SIZE) -fstack-protector-all
LDFLAGS+=-T $(ARCH_DIR)/$(ARCH).ld -nostdlib

# These are special linker flags needed for SMP bootstrap code on i686 architecture.
ifeq ($(SMP), 1)
ifeq ($(ARCH), i686)
AP_LDFLAGS:=-nostdlib -N -e _ap_start16 -Ttext 0x1000
endif
endif

# Additional libraries needed by the kernel image.
LIBS:=-lgcc

# Binary file dependencies
BINARY_DEPS:=

ifeq ($(BOOTCONSOLE), framebuffer)
BINARY_DEPS+=console_font
endif

# Header files. This variable is defined for documentation generation.
HFILES:=$(shell find $(SRC_DIR) -name '*.h')

# C source files:

# Architecture independent kernel source files:
CFILES:=$(shell find $(SRC_DIR)  -not \( -path $(SRC_DIR)/arch -prune \) -name '*.c')

# Architecture dependent kernel source files:
CFILES+=$(shell find $(ARCH_DIR) -not \( -path $(ARCH_DIR)/platform -prune \) -name '*.c')

# Architecture dependent and platform specific kernel source files:
CFILES+=$(shell find $(PLATFORM_DIR) -name '*.c')

# Assembly files:

# Architecture independent kernel source files:
ASFILES:=$(shell find $(SRC_DIR) -not \( -path $(SRC_DIR)/arch -prune \) -name '*.S')

# Architecture dependent kernel source files:
ASFILES+=$(shell find $(ARCH_DIR) -not \( -path $(ARCH_DIR)/platform -prune \) -name '*.S')

# Architecture dependent and platform specific kernel source files:
ASFILES+=$(shell find $(PLATFORM_DIR) -name '*.S')

ifneq ($(BOOTCONSOLE), framebuffer)
CFILES:=$(filter-out $(ARCH_DIR)/bootconsole/bootconsole_framebuffer.c, $(CFILES))
endif

# The i686 SMP bootstrap code needs to be compiled differently from ordinary source files
# so it is excluded from the normal compilation list.
ifeq ($(ARCH), i686)
ASFILES:=$(filter-out $(ARCH_DIR)/boot/boot_ap.S, $(ASFILES))
endif

# Object files:
OBJS:=$(patsubst %.c, $(OBJ_DIR)/%.o, $(notdir $(CFILES)))
OBJS+=$(patsubst %.S, $(OBJ_DIR)/%.o, $(notdir $(ASFILES)))

ifeq ($(ARCH), i686)
ifeq ($(SMP), 1)
OBJS:=$(filter-out $(OBJ_DIR)/boot_ap.o, $(OBJS))
endif
endif

# Objects to link to form the final kernel executable image.
LINK_LIST:=\
$(OBJS)\
$(LIBS)

# This is needed because the compilation of files is split into different directiories.
# It adds additional directories to search for missing dependencies.
VPATH=$(sort $(dir $(CFILES)) $(dir $(ASFILES)))

# This includes the dependency files generated by the compiler with the -MMP flag.
# The - at the beginning is to prevent errors if the dependency files don't exist (As is on a clean run).
-include $(OBJS:.o=.d)

.PHONY : all iso qemu clean

# This is so that make by default doesn't try to recompile the first source files on successive calls.
.DEFAULT_GOAL:=all

all : $(BIN_DIR)/yauos

iso : $(ISO_DIR)/yauos.iso

# Default rule for compiling C files.
$(OBJ_DIR)/%.o : %.c
	@mkdir -p $(OBJ_DIR)
	$(CC) -c $< -o $@ $(CFLAGS)

# Default rule for compiling .S assembly files.
$(OBJ_DIR)/%.o : %.S
	@mkdir -p $(OBJ_DIR)
	$(CC) -c $< -o $@ $(CFLAGS)

# The SMP trampoline code is compiled separately and is linked to the final kernel image as a binary file.
# This is so because the kernel is a 32 bit ELF binary and ELF doesn't support 16 bit code. This means that the
# trampoline code must be separately compiled as a binary file and linked to the kernel as an external binary.
ifeq ($(SMP), 1)
ifeq ($(ARCH), i686)
$(BIN_DIR)/boot_ap : $(ARCH_DIR)/boot/boot_ap.S
	@mkdir -p $(BIN_DIR)
	$(CC) $(CFLAGS) -c $< -o $(OBJ_DIR)/boot_ap.o
	$(LD) $(AP_LDFLAGS) -o $(OBJ_DIR)/boot_ap_tmp.o $(OBJ_DIR)/boot_ap.o
	$(OBJCOPY) -O binary $(OBJ_DIR)/boot_ap_tmp.o $(BIN_DIR)/boot_ap
endif
endif

# This rule generates a rudimentary symbol table with only function symbols for kernel stack traces.
# Later on DWARF support is planned and this will be no more. This is loaded as a multiboot2 module.-s
ifeq ($(SMP), 1)
$(OBJ_DIR)/symtab.txt : $(BIN_DIR)/yauos
	$(NM) $(BIN_DIR)/yauos | awk '$$2=="T" || $$2=="t" {printf "%s=%s\n", $$1, $$3}' > $(OBJ_DIR)/symtab.txt
	$(NM) $(OBJ_DIR)/boot_ap_tmp.o | awk '$$2=="T" || $$2=="t" {printf "%s=%s\n", $$1, $$3}' >> $(OBJ_DIR)/symtab.txt
else
$(OBJ_DIR)/symtab.txt : $(BIN_DIR)/yauos
	$(NM) $(BIN_DIR)/yauos | awk '$$2=="T" || $$2=="t" {printf "%s=%s\n", $$1, $$3}' > $(OBJ_DIR)/symtab.txt
endif

# Kernel elf executable main rules.
# The additional files passed to the linker through -Xlinker
# are copied to the current directory because ld would include their full path
# in the exported symbol names in the binary image.
# Main rule for building the kernel for 32 bit i686.
# On this architecture we rely on a multiboot 2 compliant bootloader such as GRUB.
ifeq ($(ARCH), i686)
ifeq ($(SMP), 1)
$(BIN_DIR)/yauos : $(OBJS) $(ARCH_DIR)/$(ARCH).ld $(BINARY_DEPS) $(BIN_DIR)/boot_ap
	@mkdir -p $(BIN_DIR)
	@cp $(BIN_DIR)/boot_ap $(SRC_DIR)/
	$(CC) -o $@ $(CFLAGS) $(LDFLAGS) $(LINK_LIST) -Xlinker -b binary $(BINARY_DEPS) boot_ap
	@grub-file --is-x86-multiboot2 $(BIN_DIR)/yauos
	@rm -rf $(SRC_DIR)/boot_ap
else
$(BIN_DIR)/yauos : $(OBJS) $(ARCH_DIR)/$(ARCH).ld $(BINARY_DEPS)
	@mkdir -p $(BIN_DIR)
	$(CC) -o $@ $(CFLAGS) $(LDFLAGS) $(LINK_LIST) -Xlinker -b binary $(BINARY_DEPS)
	@grub-file --is-x86-multiboot2 $(BIN_DIR)/yauos
endif
endif

# ISO generation rule. This make a complete bootable ISO image by bundling the kernel ELF image with GRUB2.
# This enables the kernel to be flashed on a USB driver and tested on real hardware.
# This is also the only way to boot the kernel on Qemu, because the kernel relies on the multiboot2 specification
# but Qemu can only boot multiboot (1) ELF kernels directly with the -kernel option.
ifeq ($(ARCH), i686)
$(ISO_DIR)/yauos.iso : $(BIN_DIR)/yauos $(OBJ_DIR)/symtab.txt
	@mkdir -p $(ISO_DIR) $(PROJECT_DIR)/tmp $(PROJECT_DIR)/tmp/boot $(PROJECT_DIR)/tmp/boot/grub
	@cp $(BIN_DIR)/yauos $(PROJECT_DIR)/tmp/boot
	@cp $(OBJ_DIR)/symtab.txt $(PROJECT_DIR)/tmp/boot
	@echo -e > $(PROJECT_DIR)/tmp/boot/grub/grub.cfg 'set timeout=4\nset default=0\nmenuentry "yauos" {\n\tmultiboot2 /boot/yauos bootconsole=$(BOOTCONSOLE)\n\tmodule2 /boot/symtab.txt symbol_table}'
	@grub-mkrescue -o $(ISO_DIR)/yauos.iso $(PROJECT_DIR)/tmp
	@rm -rf $(PROJECT_DIR)/tmp
endif

# Rules for running on the Qemu emulator.
# If bootconsole is set to serial, all the output will go to stdout and to a file for later inspection.
# This is the reccomended way for now due to the kernel not yet supporting manual console scrolling, to avoid losing output.
ifeq ($(BOOTCONSOLE), serial)
qemu : $(ISO_DIR)/yauos.iso
	@rm -rf $(LOG_DIR)/*
	qemu-system-$(QEMU_ARCH) $(QEMU_FLAGS)
else
qemu : $(ISO_DIR)/yauos.iso
	qemu-system-$(QEMU_ARCH) $(QEMU_FLAGS)
endif

# This target is what the GNU docs call an empty target and it's made so that the doxygen documentation is generated only when any of the source files change.
# Since doxygen does not directly support assembly files, the dependency list does not include them.
# This will be fixed later on.
# The documentation is genereated as a PDF file and stored in the docs folder.
docs : $(CFILES) $(HFILES) $(PROJECT_DIR)/Doxyfile
	doxygen $(PROJECT_DIR)/Doxyfile
	@cd $(PROJECT_DIR)/docs/latex && make
	@mv $(PROJECT_DIR)/docs/latex/refman.pdf $(PROJECT_DIR)/docs/yauos.pdf
	@rm -rf $(PROJECT_DIR)/docs/latex/
	@touch docs

# Clean target. Removes everything that has been generated, including documentation. Make sure the variables used here are defined and not empty or your whole root filesystem might be erased (granted make is run with super user priviliges) or only your home directory (more likely due to make being run as your user usually).
clean:
	@rm -f iso docs
	@rm -rf $(BIN_DIR)/* $(OBJ_DIR)/* $(ISO_DIR)/* $(DOC_DIR)/* $(LOG_DIR)/*
