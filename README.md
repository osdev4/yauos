YauOS (Yet another Unix-like OS) is an hobby operating system i'm developing for educational purposes.   
The project is still in its earliest stages and although not much functionality is yet found on this kernel, i have already added build instructions for those who might want to try and play with this.
You can find them on the [wiki](TODO)

Project goals:
- Being a Unix like OS with Posix compliance.
- Support for the x86 (i686+) 32 bit architecture only.
- Support for booting only on legacy BIOS systems (no UEFI support).
- Support only for PC-AT platform.
- Support for SMP only with the old MP specification (no ACPI support).
- Being able to boot and work properly on real hardware (although mostly old hardware).

Given the specs above, there needs to be added that this OS mostly targets old computers.

The rationale for the above is that:
- I've got old hardware with those specs which i can test on (mostly old Thinkpads).
- There's far more documentation for this old stuff on the web.
- New hardware specifications are far more complex and lack proper documentation for a hobbyst project like this.

Next, i want to give credit to the poeple at the [OSDev](https://www.osdev.org/) community for having such an exhaustive library of information on their [wiki](https://wiki.osdev.org/) and [forums](https://forum.osdev.org/).

Finally, i want to put here recent news about this OS.

Currently YauOS:
- Can boot on qemu and real hardware by using GRUB or any multiboot 2 compliant bootloader (it is  not excluded that in the future a custom bootloader and boot protocol might be developed for YauOS, but it is not a primary goal for now).
  If your BIOS supports it, you can boot it from a USB flash drive by burning the ISO
  to the drive.
- Basic in-kernel printing with VGA text mode (80x25), framebuffer text mode (statically defined default of 1366x768) or to serial port.
- Basic exception and interrupt handling for single core CPUs with PIC and PIT.
- Basic exception and interrupt handling for multi core CPUS with APIC and IOAPIC.
- Stack tracing for kernel exceptions with symbol resolution (basic resolution not based on DWARF for now).
- SMP.

YauOS wish list:
- Finish developing basic hardware abstraction layer infrastructure (exceptions, interrupts, interprocessor interrupts, timers, low level memory management).
- Build on top of that the VFS layer and implement a proper device filesystem and have a proper I/O subsystem.
- Add basic RAM disk driver to start implementing file systems.
- Add ext2 filesystem support.
- Add task management support.
- Add user space support.
- Add ELF loading support for user space programs.
- Add ATA IDE driver.
- Add DWARF support for in-kernel debugging and symbol resolution.

The above are considered basic functionality features that this OS wishes to get in the not too distant future to start looking like more of a real OS.

Distant future goals are:
- Add PCI support.
- Add USB support.
- Add CDROM support.
- Add ethernet driver.
- Add networking support.
- Add shared libraries support for user space.
- Add native compiler support (become self hosting).
- Add module loading support (in-kernel shared libraries).

After the above:
- Start porting POSIX software to YauOS.

Final notes:
- If you wish to get involved or build upon this, there are some instructions to get you started on the project's [wiki](TODO).